FROM node:16.14-alpine

# EXPOSE 8003

WORKDIR /var/www/service/

COPY . /var/www/service

# RUN chown -R node:node /var/www/service
# USER node
# Install requirements
# RUN yarn

# Run service
# CMD ["yarn", "start"]
