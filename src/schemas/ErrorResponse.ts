import { IsNumber, IsObject, IsOptional, IsString } from "class-validator";

export class ErrorResponse {
  @IsString()
  public declare status: string;

  @IsString()
  public declare error: string;

  @IsOptional()
  @IsObject()
  public declare data: unknown;

  @IsNumber()
  public declare timestamp: number;
}
