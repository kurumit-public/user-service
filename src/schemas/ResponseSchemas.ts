/* eslint-disable @typescript-eslint/ban-types */
import { ErrorResponse } from "@schemas";
import { ErrorsCodeEnum } from "@helpers";

interface IDescription {
  statusCode: number;
  description: string;
}

interface IOptions {
  contentType?: string;
  description?: string;
  statusCode?: string | number;
  isArray?: boolean;
}

interface IArgs {
  responseClass: Function | string;
  options: IOptions;
}

type ResponseType = [string | Function, IDescription];

const notFoundDescription: IDescription = {
  statusCode: 404,
  description: ErrorsCodeEnum.ENTITY_NOT_FOUND,
};

const notPermissionsDescription: IDescription = {
  statusCode: 403,
  description: ErrorsCodeEnum.USER_NOT_HAVE_PERMISSIONS,
};

const notAuthDescription: IDescription = {
  statusCode: 401,
  description: ErrorsCodeEnum.USER_NOT_AUTHORIZED,
};

const notValidDescription: IDescription = {
  statusCode: 422,
  description: ErrorsCodeEnum.VALIDATION_ERROR,
};

export const NotFound: ResponseType = [ErrorResponse, notFoundDescription];

export const NotPermissions: ResponseType = [ErrorResponse, notPermissionsDescription];

export const NotAuth: ResponseType = [ErrorResponse, notAuthDescription];

export const NotValid: ResponseType = [ErrorResponse, notValidDescription];

export const Test: IArgs = { responseClass: ErrorResponse, options: notValidDescription };
