import { Type } from "class-transformer";
import {
  IsDateString,
  IsInt,
  IsNumber,
  IsOptional,
  IsPositive,
  IsString,
  ValidateNested,
} from "class-validator";

class PictureData {
  @IsInt()
  @IsPositive()
  id!: number;

  @IsInt()
  @IsPositive()
  profile_id!: number;

  @IsString()
  hash!: string;

  @IsString()
  extension!: string;

  @IsPositive()
  @IsNumber()
  size!: number;

  @IsPositive()
  @IsInt()
  resolution_width!: number;

  @IsPositive()
  @IsInt()
  resolution_height!: number;

  @IsDateString()
  created_at!: string;

  @IsOptional()
  @IsDateString()
  updated_at!: string;

  @IsOptional()
  @IsDateString()
  deleted_at!: string;
}

export default class PictureSchema {
  @IsOptional()
  @ValidateNested()
  @Type(() => PictureData)
  size16_16?: PictureData;

  @IsOptional()
  @ValidateNested()
  @Type(() => PictureData)
  size64_64?: PictureData;

  @IsOptional()
  @ValidateNested()
  @Type(() => PictureData)
  size120_120?: PictureData;

  @IsOptional()
  @ValidateNested()
  @Type(() => PictureData)
  size160_160?: PictureData;

  @IsOptional()
  @ValidateNested()
  @Type(() => PictureData)
  size300_300?: PictureData;
}
