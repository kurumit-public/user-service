import { User } from "@entities";

const usersMapper = (users: User[]): unknown[] => {
  return users.map((user) => {
    return {
      ...user,
      accesses: user.accesses.map(({ access }) => access.code),
      profile: {
        ...user.profile,
        pictures: {
          size16_16: user.profile.pictures?.find((picture) => picture.resolution_width === 16),
          size64_64: user.profile.pictures?.find((picture) => picture.resolution_width === 64),
          size120_120: user.profile.pictures?.find((picture) => picture.resolution_width === 120),
          size160_160: user.profile.pictures?.find((picture) => picture.resolution_width === 160),
          size300_300: user.profile.pictures?.find((picture) => picture.resolution_width === 300),
        },
      },
    };
  });
};

export default usersMapper;
