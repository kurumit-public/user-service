import { User } from "@entities";

interface IPictureData {
  id: number;
  profile_id: number;
  hash: string;
  extension: string;
  size: number;
  resolution_width: number;
  resolution_height: number;
  created_at: string;
  updated_at?: string;
  deleted_at?: string;
}

interface IPicture {
  size16_16?: IPictureData;
  size64_64?: IPictureData;
  size120_120?: IPictureData;
  size160_160?: IPictureData;
  size300_300?: IPictureData;
}

interface IProfile {
  [key: string]: unknown;
  pictures?: IPicture;
}

interface IUser {
  [key: string]: unknown;
  accesses: string[];
  profile: IProfile;
}

const userMapper = (user: User): IUser => {
  return {
    ...user,
    accesses: user.accesses.map(({ access }) => access.code),
    profile: {
      ...user.profile,
      pictures: {
        size16_16: user.profile.pictures?.find((picture) => picture.resolution_width === 16),
        size64_64: user.profile.pictures?.find((picture) => picture.resolution_width === 64),
        size120_120: user.profile.pictures?.find((picture) => picture.resolution_width === 120),
        size160_160: user.profile.pictures?.find((picture) => picture.resolution_width === 160),
        size300_300: user.profile.pictures?.find((picture) => picture.resolution_width === 300),
      },
    },
  };
};

export default userMapper;
