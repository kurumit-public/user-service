import { Context } from "koa";
import { BadRequestError, KoaMiddlewareInterface, Middleware } from "routing-controllers";
import { Service } from "typedi";
import { ValidationError } from "class-validator";

import { ErrorHandler } from "@helpers";

import { IValidateData, IValidateErrors } from "@types";

@Service()
@Middleware({ type: "before", priority: 50 })
export default class ErrorHandlerMiddleware implements KoaMiddlewareInterface {
  private getConstraintsRows(storage: string[], errors?: ValidationError[]): void {
    if (!errors) return;

    errors.forEach((item) => {
      for (const key in item) {
        if (key === "constraints") {
          for (const validator in item.constraints) {
            storage.push(`${validator}: ${item.constraints[validator]}`);
          }
        }
      }

      if (item.children) {
        this.getConstraintsRows(storage, item.children);
      }
    });
  }

  private getConstraints(storage: string[]): string {
    return storage.join(". ");
  }

  private generateValidationErrors(Exception: IValidateErrors): IValidateData[] {
    const errors: IValidateData[] = [];

    if (Exception.errors) {
      const constraintsStorage: string[] = [];

      Exception.errors.forEach((error: ValidationError) => {
        let errors_description!: string[];

        if (error.constraints) {
          errors_description = Object.values(error.constraints);
        }

        this.getConstraintsRows(constraintsStorage, error.children);

        errors.push({
          field: error.property,
          value: error.value,
          description: errors_description,
          child_constraints: this.getConstraints(constraintsStorage),
        });
      });
    }
    return errors;
  }

  public async use(ctx: Context, next: (err?: unknown) => Promise<unknown>): Promise<void> {
    try {
      await next();
    } catch (Exception: unknown) {
      if (Exception instanceof ErrorHandler) {
        ctx.body = {
          status: "error",
          error: Exception.message,
          data: Exception?.data,
          timestamp: new Date().getTime(),
        };
        ctx.status = Exception.statusCode;
      } else if (Exception instanceof BadRequestError) {
        const errors = this.generateValidationErrors(Exception);

        ctx.body = {
          status: "error",
          error: errors.length
            ? `validation error: ${Exception.message}`
            : `bad request: ${Exception.message}`,
          data: errors.length ? errors : null,
          timestamp: new Date().getTime(),
        };
        ctx.status = 422;
      } else {
        throw Exception;
      }
    }
  }
}
