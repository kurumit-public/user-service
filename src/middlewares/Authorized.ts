import fs from "fs";
import jwt from "jsonwebtoken";
import { Action } from "routing-controllers";

import { ErrorsCodeEnum, HttpErrors, ErrorHandler } from "@helpers";

import { IUser } from "@types";

export default class Authorized {
  private readonly jwtUser: null | IUser | void;

  constructor(action: Action) {
    if (!action.request.header.authorization) {
      const errorInfo = new HttpErrors(ErrorsCodeEnum.USER_NOT_AUTHORIZED);
      throw new ErrorHandler(errorInfo);
    }
    const token =
      action.request.header["authorization"].match(/Bearer (?<token>.*)/)?.groups?.token;

    const publicKey = String(fs.readFileSync(".ssh/id_rsa"));
    if (token) {
      this.jwtUser = jwt.verify(token, publicKey, { algorithms: ["HS256"] }, (err, decoded) => {
        if (err) {
          const errorInfo = new HttpErrors(ErrorsCodeEnum.USER_NOT_AUTHORIZED);
          throw new ErrorHandler(errorInfo);
        } else {
          return decoded;
        }
      });
    }
  }

  public async check(): Promise<boolean> {
    return !!this.jwtUser;
  }

  public async user(): Promise<null | IUser | void> {
    return this.jwtUser;
  }

  public static async check(action: Action): Promise<boolean> {
    return new Authorized(action).check();
  }

  public static async user(action: Action): Promise<null | IUser | void> {
    return new Authorized(action).user();
  }
}
