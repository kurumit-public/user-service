/* eslint-disable @typescript-eslint/ban-types */
import { Repository } from "typeorm";

export interface IQueryParams {
  limit?: number;
  sort_type?: string;
  sort_field?: string;
  offset?: number;
}

export interface IQueryData {
  relations?: string[];
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  whereFields?: Record<string, any>;
}

export interface IQueryConstructor {
  (query: {}): IQueryData;
}

export interface ISearchParamArg<T> {
  repository: Repository<T>;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  query: Record<string, any> & IQueryParams;
  strictFields?: string[];
  queryConstructor?: IQueryConstructor;
}
