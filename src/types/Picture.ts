export interface IPicture {
  hash: string;
  extension: string;
  size: number;
  resolution_width: number;
  resolution_height: number;
}
