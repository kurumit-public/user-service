import { ValidationError } from "class-validator";
import { BadRequestError } from "routing-controllers";

export interface IValidateData {
  field: string;
  value: string;
  description?: string[];
  child_constraints: string;
}

export interface IValidateErrors extends BadRequestError {
  errors?: ValidationError[];
}
