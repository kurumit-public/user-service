import { Paginator } from "@types";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export interface Response<T = any> {
  status?: string;
  data?: T;
  paginator?: Paginator;
  error?: string;
  timestamp?: number;
}
