export interface IProject {
  id: number;
  name: string;
  domain: string;
  start_at: string;
  end_at: string;
  type_id: number;
  type: IProjectType;
  active_type: IProjectType;
  tags?: ITag[];
}

interface IProjectType {
  id: number;
  name: string;
  active_project_id?: number;
}

export interface ITag extends IProject {
  id: number;
  name: string;
  type_id: number;
  type: ITagType;
}

interface ITagType {
  id: number;
  name: string;
}
