export interface Paginator {
  current: number;
  count: number;
  last_page: number;
}
