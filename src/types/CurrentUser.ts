interface IPicture {
  hash: string;
  extension: string;
  size: number;
  resolution: number;
}

interface IUserData {
  id: number;
  firstname?: string;
  lastname?: string;
  pictures?: IPicture[];
  email: string;
}

interface IContext {
  accesses: string[];
  user: IUserData;
}

export interface IUser {
  aud: string;
  expiredAt: number;
  signedAt: number;
  context: IContext;
}
