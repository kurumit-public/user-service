export interface IErrorInfo {
  code: number;
  message: string;
}
