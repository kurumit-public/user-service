import { IsEmail, IsInt, IsPositive, ValidateNested } from "class-validator";
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  Entity,
  Exclusion,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
} from "typeorm";
import { Type } from "class-transformer";

import Encryptor from "../helpers/Encryptor";
import Template from "./Template";
import {
  UserToAccess,
  Phone,
  GeoIp,
  Profile,
  NetworkingProfile,
  Referral,
  Creator,
  Project,
} from "./index";

@Exclusion(`CONCAT(profile.firstname, ' ', profile.lastname) as 'fullname'`)
@Entity({ name: "users" })
export default class User extends Template {
  @IsEmail()
  @Column({ type: "varchar", length: 256, unique: true })
  email!: string;

  @Column({ type: "varchar", length: 256, select: false })
  password!: string;

  @IsInt()
  @IsPositive()
  @Column({ type: "integer", nullable: true })
  creator_id!: number;

  @ValidateNested()
  @Type(() => Profile)
  @OneToOne(() => Profile, (entity) => entity.user, { eager: true, cascade: ["insert", "update"] })
  profile!: Profile;

  @ValidateNested()
  @Type(() => Phone)
  @OneToOne(() => Phone, (entity) => entity.user, { eager: true, cascade: ["insert", "update"] })
  phone!: Phone;

  @ManyToOne(() => Creator, (entity) => entity.users, { eager: true })
  @JoinColumn({ name: "creator_id" })
  creator!: Creator;

  @OneToMany(() => GeoIp, (entity) => entity.user)
  geo_ips!: GeoIp[];

  @ValidateNested({ each: true })
  @Type(() => Referral)
  @OneToMany(() => Referral, (entity) => entity.user, { eager: true })
  referrals!: Referral[];

  @ValidateNested({ each: true })
  @Type(() => NetworkingProfile)
  @OneToMany(() => NetworkingProfile, (entity) => entity.user, {
    eager: true,
    cascade: ["insert", "update"],
  })
  networking_profiles!: NetworkingProfile[];

  @ValidateNested({ each: true })
  @Type(() => String)
  @OneToMany(() => UserToAccess, (entity) => entity.user, {
    eager: true,
    cascade: ["insert", "update"],
  })
  accesses!: UserToAccess[];

  @ValidateNested({ each: true })
  @Type(() => Project)
  @OneToMany(() => Project, (entity) => entity.user, { eager: true })
  projects!: Project[];

  @BeforeInsert()
  @BeforeUpdate()
  hashPassword(): void {
    if (!this.password) return;

    const hash = Encryptor.createHash(this.password);
    this.password = hash;
  }
}
