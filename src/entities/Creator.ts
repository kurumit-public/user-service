import { IsDateString, IsInt, IsPositive } from "class-validator";
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  Timestamp,
} from "typeorm";

import { User } from "./index";

@Entity({ name: "user_creators" })
export default class Creator extends BaseEntity {
  @IsInt()
  @IsPositive()
  @PrimaryGeneratedColumn({ type: "integer" })
  id!: number;

  @IsInt()
  @IsPositive()
  @Column({ type: "integer" })
  user_id!: number;

  @OneToMany(() => User, (entity) => entity.creator)
  @JoinColumn({ name: "user_id" })
  users!: User[];

  @OneToOne(() => User)
  @JoinColumn({ name: "user_id" })
  user!: User;

  @IsDateString()
  @CreateDateColumn({ type: "timestamp" })
  created_at!: Timestamp;
}
