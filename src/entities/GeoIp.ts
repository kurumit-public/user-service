import { IsDateString, IsInt, IsPositive, IsString } from "class-validator";
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  Timestamp,
} from "typeorm";

import { User } from "./index";

@Entity({ name: "user_geo_ips" })
export default class GeoIp extends BaseEntity {
  @IsInt()
  @IsPositive()
  @PrimaryGeneratedColumn({ type: "integer" })
  id!: number;

  @IsInt()
  @IsPositive()
  @Column({ type: "bigint" })
  user_id!: number;

  @IsString()
  @Column({ type: "varchar", length: 256 })
  country!: string;

  @IsString()
  @Column({ type: "varchar", length: 256 })
  timezone!: string;

  @IsString()
  @Column({ type: "varchar", length: 256 })
  city!: string;

  @IsString()
  @Column({ type: "varchar", length: 256 })
  region!: string;

  @ManyToOne(() => User, (entity) => entity.geo_ips)
  @JoinColumn({ name: "user_id" })
  user!: User;

  @IsDateString()
  @CreateDateColumn({ type: "timestamp" })
  created_at!: Timestamp;
}
