import { IsBoolean, IsInt, IsPositive, IsString } from "class-validator";
import { Column, Entity, JoinColumn, OneToOne } from "typeorm";

import { User } from "./index";
import Template from "./Template";

@Entity({ name: "user_phones" })
export default class Phone extends Template {
  @IsInt()
  @IsPositive()
  @Column({ type: "integer" })
  user_id!: number;

  @IsString()
  @Column({ type: "varchar", length: 256 })
  number!: string;

  @IsBoolean()
  @Column({ type: "boolean" })
  verified!: boolean;

  @OneToOne(() => User, (entity) => entity.phone)
  @JoinColumn({ name: "user_id" })
  user!: User;
}
