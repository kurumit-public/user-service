import { Type } from "class-transformer";
import { IsBoolean, IsInt, IsPositive, ValidateNested } from "class-validator";
import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany } from "typeorm";

import { User, NetworkingToTags } from "./index";
import Template from "./Template";

@Entity({ name: "networking_profiles" })
export default class NetworkingProfile extends Template {
  @IsInt()
  @IsPositive()
  @Column({ type: "integer" })
  user_id!: number;

  @IsInt()
  @IsPositive()
  @Index()
  @Column({ type: "integer" })
  project_id!: number;

  @IsBoolean()
  @Column({ type: "boolean" })
  enabled!: boolean;

  @ManyToOne(() => User, (entity) => entity.networking_profiles)
  @JoinColumn({ name: "user_id" })
  user!: User;

  @ValidateNested({ each: true })
  @Type(() => NetworkingToTags)
  @OneToMany(() => NetworkingToTags, (entity) => entity.networking_profile, {
    eager: true,
    cascade: ["insert", "update"],
  })
  tags!: NetworkingToTags[];
}
