import { IsDateString, IsInt, IsOptional, IsPositive } from "class-validator";
import {
  BaseEntity,
  CreateDateColumn,
  DeleteDateColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

export default abstract class Template extends BaseEntity {
  @IsInt()
  @IsPositive()
  @PrimaryGeneratedColumn({ type: "integer" })
  id!: number;

  @IsDateString()
  @CreateDateColumn({ type: "timestamp" })
  created_at!: string;

  @IsOptional()
  @IsDateString()
  @UpdateDateColumn({ type: "timestamp" })
  updated_at?: string;

  @IsOptional()
  @IsDateString()
  @DeleteDateColumn({ type: "timestamp" })
  deleted_at?: string;
}
