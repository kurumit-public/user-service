import { IsBoolean, IsInt, IsPositive } from "class-validator";
import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";

import { User } from "./index";
import Template from "./Template";

@Entity({ name: "user_projects" })
export default class Project extends Template {
  @IsInt()
  @IsPositive()
  @Column({ type: "integer" })
  user_id!: number;

  @IsInt()
  @IsPositive()
  @Index()
  @Column({ type: "integer" })
  project_id!: number;

  @IsBoolean()
  @Column({ type: "boolean", default: false })
  confirmed!: boolean;

  @ManyToOne(() => User, (entity) => entity.projects)
  @JoinColumn({ name: "user_id" })
  user!: User;
}
