import { IsInt, IsPositive } from "class-validator";
import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";

import { User, Access } from "./index";
import Template from "./Template";

@Entity({ name: "user_to_access" })
export default class UserToAccess extends Template {
  @IsInt()
  @IsPositive()
  @Column({ type: "integer" })
  user_id!: number;

  @IsInt()
  @IsPositive()
  @Column({ type: "integer" })
  access_id!: number;

  @IsInt()
  @IsPositive()
  @Index()
  @Column({ type: "integer", nullable: true })
  project_id!: number;

  @ManyToOne(() => User, (entity) => entity.accesses)
  @JoinColumn({ name: "user_id" })
  user!: User;

  @ManyToOne(() => Access, (entity) => entity.accesses, { eager: true })
  @JoinColumn({ name: "access_id" })
  access!: Access;
}
