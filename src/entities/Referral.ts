import { IsDateString, IsInt, IsPositive } from "class-validator";
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  Timestamp,
} from "typeorm";

import { User } from "./index";

@Entity({ name: "user_referrals" })
export default class Referral extends BaseEntity {
  @IsInt()
  @IsPositive()
  @PrimaryGeneratedColumn({ type: "integer" })
  id!: number;

  @IsInt()
  @IsPositive()
  @Column({ type: "integer" })
  user_id!: number;

  @IsInt()
  @IsPositive()
  @Column({ type: "integer" })
  referral_id!: number;

  @ManyToOne(() => User, (entity) => entity.referrals)
  @JoinColumn({ name: "referral_id" })
  referral!: User;

  @ManyToOne(() => User)
  @JoinColumn({ name: "user_id" })
  user!: User;

  @IsDateString()
  @CreateDateColumn({ type: "timestamp" })
  created_at!: Timestamp;
}
