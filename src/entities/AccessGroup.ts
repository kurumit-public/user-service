import { Type } from "class-transformer";
import { IsString, ValidateNested } from "class-validator";
import { Column, Entity, JoinTable, ManyToMany } from "typeorm";
import { Access } from "./index";

import Template from "./Template";

@Entity({ name: "access_groups" })
export default class AccessGroup extends Template {
  @IsString()
  @Column({ type: "varchar", length: 128 })
  name!: string;

  @ValidateNested({ each: true })
  @Type(() => Access)
  @ManyToMany(() => Access, (entity) => entity.groups, {
    eager: true,
    cascade: ["insert", "update"],
  })
  @JoinTable({
    name: "access_to_group",
    joinColumn: {
      name: "group_id",
      referencedColumnName: "id",
    },
    inverseJoinColumn: {
      name: "access_id",
      referencedColumnName: "id",
    },
  })
  accesses!: Access[];
}
