import { IsInt, IsPositive } from "class-validator";
import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";

import { NetworkingProfile } from "./index";
import Template from "./Template";

@Entity({ name: "networking_to_tags" })
export default class NetworkingToTags extends Template {
  @IsInt()
  @IsPositive()
  @Index()
  @Column({ type: "integer" })
  tag_id!: number;

  @IsInt()
  @IsPositive()
  @Column({ type: "integer" })
  networking_profile_id!: number;

  @ManyToOne(() => NetworkingProfile, (entity) => entity.tags)
  @JoinColumn({ name: "networking_profile_id" })
  networking_profile!: NetworkingProfile[];
}
