import { IsInt, IsNumber, IsPositive, IsString } from "class-validator";
import { Column, Entity, JoinColumn, ManyToOne } from "typeorm";

import { Profile } from "./index";
import Template from "./Template";

@Entity({ name: "profile_pictures" })
export default class Picture extends Template {
  @IsInt()
  @IsPositive()
  @Column({ type: "integer" })
  profile_id!: number;

  @IsString()
  @Column({ type: "varchar" })
  hash!: string;

  @IsString()
  @Column({ type: "varchar" })
  extension!: string;

  @IsPositive()
  @IsNumber()
  @Column({ type: "numeric" })
  size!: number;

  @IsInt()
  @IsPositive()
  @Column({ type: "integer" })
  resolution_width!: number;

  @IsInt()
  @IsPositive()
  @Column({ type: "integer" })
  resolution_height!: number;

  @ManyToOne(() => Profile, (entity) => entity.pictures)
  @JoinColumn({ name: "profile_id" })
  profile!: Profile;
}
