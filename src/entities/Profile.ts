import { Type } from "class-transformer";
import { IsInt, IsOptional, IsPositive, IsString, ValidateNested } from "class-validator";
import { Column, Entity, JoinColumn, OneToMany, OneToOne } from "typeorm";

import { User, Picture } from "./index";
import Template from "./Template";
import PictureSchema  from "../schemas/Picture";

@Entity({ name: "user_profiles" })
export default class Profile extends Template {
  @IsInt()
  @IsPositive()
  @Column({ type: "integer" })
  user_id!: number;

  @IsOptional()
  @IsString()
  @Column({ type: "varchar", length: 256, nullable: true })
  firstname?: string;

  @IsOptional()
  @IsString()
  @Column({ type: "varchar", length: 256, nullable: true })
  lastname?: string;

  @IsOptional()
  @IsString()
  @Column({ type: "varchar", length: 256, nullable: true })
  patronymic?: string;

  @IsOptional()
  @IsString()
  @Column({ type: "text", nullable: true })
  description!: string;

  @IsOptional()
  @IsInt()
  @IsPositive()
  @Column({ type: "integer", nullable: true })
  country_id?: number;

  @IsOptional()
  @IsInt()
  @IsPositive()
  @Column({ type: "integer", nullable: true })
  city_id?: number;

  @IsOptional()
  @IsInt()
  @IsPositive()
  @Column({ type: "integer", nullable: true })
  timezone_id?: number;

  @OneToOne(() => User, (entity) => entity.profile, { cascade: ["insert", "update"] })
  @JoinColumn({ name: "user_id" })
  user!: User;

  @IsOptional()
  @ValidateNested()
  @Type(() => PictureSchema)
  @OneToMany(() => Picture, (entity) => entity.profile, {
    eager: true,
    cascade: ["insert", "update"],
  })
  pictures?: Picture[];
}
