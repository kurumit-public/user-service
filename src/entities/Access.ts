import { IsInt, IsPositive, IsString } from "class-validator";
import { BaseEntity, Column, Entity, ManyToMany, OneToMany, PrimaryColumn } from "typeorm";

import { UserToAccess, AccessGroup } from "./index";

@Entity({ name: "user_accesses" })
export default class Access extends BaseEntity {
  @IsInt()
  @IsPositive()
  @PrimaryColumn({ type: "integer" })
  id!: number;

  @IsString()
  @Column({ type: "varchar", length: 128 })
  name!: string;

  @IsString()
  @Column({ type: "varchar", length: 128 })
  code!: string;

  @OneToMany(() => UserToAccess, (entity) => entity.access)
  accesses!: UserToAccess;

  @ManyToMany(() => AccessGroup, (entity) => entity.accesses)
  groups!: AccessGroup[];
}
