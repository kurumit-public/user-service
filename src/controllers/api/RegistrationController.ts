import {
  JsonController,
  Post,
  Body,
  Ctx,
  HttpCode,
  Get,
  CurrentUser,
  Patch,
  QueryParams,
} from "routing-controllers";
import { OpenAPI, ResponseSchema } from "routing-controllers-openapi";
import { Service } from "typedi";
import { InjectRepository } from "typeorm-typedi-extensions";
import { Context } from "koa";

import * as Mappers from "@mappers";
import * as Services from "@services";
import * as Repositories from "@repositories";
import * as Entities from "@entities";
import Controller from "@controllers/Controller";
import { Auth, ErrorsCodeEnum, HttpErrors, filterUserUpdateDto } from "@helpers";
import { Schema } from "@schemas";

import { IUser, Response } from "@types";
import * as Dto from "@dto";
import * as Queries from "@queries";

@JsonController("/auth")
@Service()
export default class RegistrationController extends Controller {
  constructor(
    @InjectRepository() private userRepository: Repositories.User,
    private userService: Services.User,
    private projectService: Services.Project,
    private geoIpService: Services.GeoIp,
    private commonService: Services.Common,
    private referralService: Services.Referral
  ) {
    super();
  }

  @OpenAPI({
    description: "Register a new user",
    summary: "Register a new user",
  })
  @ResponseSchema(...Schema.NotValid)
  @ResponseSchema(...Schema.NotFound)
  @ResponseSchema(Entities.User)
  @HttpCode(201)
  @Post("/signup")
  public async registerUser(
    @Body() dto: Dto.User,
    @Ctx() ctx: Context,
    @QueryParams({ validate: { whitelist: true } }) { inviter_id }: Queries.RegisterUser
  ): Promise<Response> {
    const projectId = 1; // заменить когда будет project service

    try {
      const oldUser = await this.userRepository.findByEmail(dto.email);

      if (oldUser) {
        throw new HttpErrors(ErrorsCodeEnum.VALIDATION_ERROR);
      }

      if (inviter_id) {
        const inviter = await this.userRepository.findOne(inviter_id);

        if (!inviter) {
          throw new HttpErrors(ErrorsCodeEnum.ENTITY_NOT_FOUND);
        }
      }

      const newUser = await this.userService.createAndSaveUser(dto, projectId);

      await this.projectService.createRelation(newUser.id, projectId, true);

      if (inviter_id) {
        await this.referralService.saveReferral(inviter_id, newUser.id);
      }

      await this.geoIpService.saveGeoData(ctx, newUser.id);

      const user = await this.userRepository.findOne(newUser.id);

      if (!user) {
        throw new HttpErrors(ErrorsCodeEnum.ENTITY_NOT_FOUND);
      }

      const filteredUser = Mappers.User(user);

      return this.response({ data: filteredUser });
    } catch (Exception) {
      if (Exception instanceof HttpErrors) {
        return this.error(Exception);
      } else {
        throw Exception;
      }
    }
  }

  @OpenAPI({
    description: "Authorize user",
    summary: "Authorize user",
  })
  @ResponseSchema(...Schema.NotFound)
  @ResponseSchema(...Schema.NotValid)
  @Post("/signin")
  public async authorizeUser(
    @Body() { email, password }: Dto.Auth,
    @Ctx() ctx: Context
  ): Promise<Response> {
    const projectId = 1; // Заменить после появления project service

    const newNetworkingTags = [1, 2, 4, 6]; // Заменить после появления project service

    try {
      const user = await this.userService.findByEmailAndPass(email, password);

      if (!user) {
        throw new HttpErrors(ErrorsCodeEnum.ENTITY_NOT_FOUND);
      }

      const isProject = this.projectService.checkProject(user, projectId);

      if (!isProject) {
        await this.projectService.createRelation(user.id, projectId);
      }

      const accesses = Mappers.User(user).accesses;

      const signedJwt = this.commonService.signinJwt(user, accesses);

      await this.geoIpService.saveGeoData(ctx, user.id);

      const networkingProfile = user.networking_profiles.find(
        (profile) => profile.project_id === projectId
      );

      if (!networkingProfile && user.networking_profiles.length) {
        await this.userService.createNetworkingProfile(user, projectId, newNetworkingTags);
      }

      return this.response({ data: signedJwt });
    } catch (Exception) {
      if (Exception instanceof HttpErrors) {
        return this.error(Exception);
      } else {
        throw Exception;
      }
    }
  }

  @OpenAPI({
    description: "Get current user",
    summary: "Get current user",
  })
  @ResponseSchema(...Schema.NotAuth)
  @ResponseSchema(...Schema.NotPermissions)
  @ResponseSchema(Entities.User)
  @Auth()
  @Get("/current")
  public async getCurrentUser(@CurrentUser() user: IUser): Promise<Response> {
    try {
      const currentUser = await this.userRepository.findOne(user.context.user.id);

      if (!currentUser) {
        throw new HttpErrors(ErrorsCodeEnum.ENTITY_NOT_FOUND);
      }

      const filteredUser = Mappers.User(currentUser);

      return this.response({ data: filteredUser });
    } catch (Exception) {
      if (Exception instanceof HttpErrors) {
        return this.error(Exception);
      } else {
        throw Exception;
      }
    }
  }

  @OpenAPI({
    description: "Find user's email",
    summary: "Find user's email",
  })
  @ResponseSchema(...Schema.NotValid)
  @ResponseSchema(...Schema.NotFound)
  @Get("/email/check")
  public async getUserByEmail(
    @QueryParams({ validate: { whitelist: true } }) { email }: Queries.GetEmail
  ): Promise<Response> {
    try {
      const user = await this.userRepository.findByEmail(email);

      if (!user) {
        throw new HttpErrors(ErrorsCodeEnum.ENTITY_NOT_FOUND);
      }

      return this.response({ data: user });
    } catch (Exception) {
      if (Exception instanceof HttpErrors) {
        return this.error(Exception);
      } else {
        throw Exception;
      }
    }
  }

  @OpenAPI({
    description: "Register a user on a new project",
    summary: "Register a new user on a new project",
  })
  @ResponseSchema(...Schema.NotValid)
  @ResponseSchema(...Schema.NotFound)
  @ResponseSchema(...Schema.NotAuth)
  @ResponseSchema(Entities.User)
  @HttpCode(201)
  @Auth()
  @Patch("/signup/project")
  public async registerUserNewProject(
    @Body() dto: Dto.UserUpdate,
    @CurrentUser() user: IUser
  ): Promise<Response> {
    const projectId = 1; // заменить когда будет project service

    try {
      const foundUser = await this.userRepository.findOne(user.context.user.id);

      if (!foundUser) {
        throw new HttpErrors(ErrorsCodeEnum.ENTITY_NOT_FOUND);
      }

      const updateResult = await this.projectService.updateRelation(
        user.context.user.id,
        projectId
      );

      if (!updateResult) {
        throw new HttpErrors(ErrorsCodeEnum.ENTITY_NOT_FOUND);
      }

      const filteredDto = filterUserUpdateDto(dto, false);

      const updatedUser = await this.userService.updateUser(foundUser, filteredDto, projectId);

      if (!updatedUser) {
        throw new HttpErrors(ErrorsCodeEnum.ENTITY_NOT_FOUND);
      }

      const filteredUser = Mappers.User(updatedUser);

      return this.response({ data: filteredUser });
    } catch (Exception) {
      if (Exception instanceof HttpErrors) {
        return this.error(Exception);
      } else {
        throw Exception;
      }
    }
  }
}
