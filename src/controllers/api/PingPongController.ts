import { JsonController, Get, HttpCode } from "routing-controllers";
import Controller from "@controllers/Controller";
import { Service } from "typedi";

import { Response } from "@types";

@JsonController()
@Service()
export default class PingPongController extends Controller {
  @HttpCode(201)
  @Get("/ping")
  public async ping(): Promise<Response> {
    return this.response({ data: "pong" });
  }
}
