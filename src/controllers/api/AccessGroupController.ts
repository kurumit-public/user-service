import {
  JsonController,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpCode,
  CurrentUser,
} from "routing-controllers";
import { OpenAPI, ResponseSchema } from "routing-controllers-openapi";
import { Service } from "typedi";
import { InjectRepository } from "typeorm-typedi-extensions";

import * as Repositories from "@repositories";
import * as Entities from "@entities";
import * as Services from "@services";
import Controller from "@controllers/Controller";
import { ErrorsCodeEnum, HttpErrors, Auth, AccessList, AccessManager } from "@helpers";
import { Schema } from "@schemas";

import { IUser, Response } from "@types";
import * as Dto from "@dto";

@Service()
@JsonController("/accesses/groups")
export default class AccessGroupController extends Controller {
  constructor(
    @InjectRepository() private groupRepository: Repositories.Group,
    @InjectRepository() private accessRepository: Repositories.Access,
    private accessService: Services.Access,
    private groupService: Services.Group
  ) {
    super();
  }

  @OpenAPI({
    description: "Create new access group",
    summary: "Create new access group",
  })
  @ResponseSchema(...Schema.NotValid)
  @ResponseSchema(...Schema.NotFound)
  @ResponseSchema(...Schema.NotAuth)
  @ResponseSchema(Entities.AccessGroup)
  @HttpCode(201)
  @Auth()
  @Post()
  public async createGroup(@Body() dto: Dto.Group, @CurrentUser() user: IUser): Promise<Response> {
    try {
      const isAccess = AccessManager.checkPermission(user, AccessList.USER_CREATE);

      if (!isAccess) {
        throw new HttpErrors(ErrorsCodeEnum.USER_NOT_HAVE_PERMISSIONS);
      }

      const Group = await this.groupRepository.findByName(dto);

      if (Group) {
        throw new HttpErrors(ErrorsCodeEnum.VALIDATION_ERROR);
      }

      const newGroup = await this.groupService.createGroup(dto);

      const Accesses = await this.accessRepository.findAccesses(dto.access_ids);

      await this.accessService.createRelation(Accesses, newGroup);

      const foundGroup = await this.groupRepository.findOne(newGroup.id);

      return this.response({ data: foundGroup });
    } catch (Exception) {
      if (Exception instanceof HttpErrors) {
        return this.error(Exception);
      } else {
        throw Exception;
      }
    }
  }

  @OpenAPI({
    description: "Get array of all access groups",
    summary: "Get array of all access groups",
  })
  @ResponseSchema(...Schema.NotAuth)
  @ResponseSchema(...Schema.NotPermissions)
  @ResponseSchema(Entities.AccessGroup, { isArray: true })
  @Auth()
  @Get()
  public async showAllGroups(@CurrentUser() user: IUser): Promise<Response> {
    try {
      const isAccess = AccessManager.checkPermission(user, AccessList.USER_CREATE);

      if (!isAccess) {
        throw new HttpErrors(ErrorsCodeEnum.USER_NOT_HAVE_PERMISSIONS);
      }

      const Group = await this.groupRepository.find();

      return this.response({ data: Group });
    } catch (Exception) {
      if (Exception instanceof HttpErrors) {
        return this.error(Exception);
      } else {
        throw Exception;
      }
    }
  }

  @OpenAPI({
    description: "Get access group by Id",
    summary: "Get access group by Id",
  })
  @ResponseSchema(...Schema.NotAuth)
  @ResponseSchema(...Schema.NotFound)
  @ResponseSchema(...Schema.NotPermissions)
  @ResponseSchema(Entities.AccessGroup)
  @Auth()
  @Get("/:group_id")
  public async showGroupById(
    @Param("group_id") group_id: number,
    @CurrentUser() user: IUser
  ): Promise<Response> {
    try {
      const isAccess = AccessManager.checkPermission(user, AccessList.USER_CREATE);

      if (!isAccess) {
        throw new HttpErrors(ErrorsCodeEnum.USER_NOT_HAVE_PERMISSIONS);
      }

      const Group = await this.groupRepository.findOne(group_id);

      if (!Group) {
        throw new HttpErrors(ErrorsCodeEnum.ENTITY_NOT_FOUND);
      }

      return this.response({ data: Group });
    } catch (Exception) {
      if (Exception instanceof HttpErrors) {
        return this.error(Exception);
      } else {
        throw Exception;
      }
    }
  }

  @OpenAPI({
    description: "Delete access group by Id",
    summary: "Delete access group by Id",
  })
  @ResponseSchema(...Schema.NotAuth)
  @ResponseSchema(...Schema.NotFound)
  @ResponseSchema(...Schema.NotPermissions)
  @ResponseSchema(Entities.AccessGroup)
  @Auth()
  @Delete("/:group_id")
  public async deleteGroup(
    @Param("group_id") group_id: number,
    @CurrentUser() user: IUser
  ): Promise<Response> {
    try {
      const isAccess = AccessManager.checkPermission(user, AccessList.USER_CREATE);

      if (!isAccess) {
        throw new HttpErrors(ErrorsCodeEnum.USER_NOT_HAVE_PERMISSIONS);
      }

      const Group = await this.groupRepository.findOne(group_id);

      if (!Group) {
        throw new HttpErrors(ErrorsCodeEnum.ENTITY_NOT_FOUND);
      }

      const deletedGroup = await this.groupRepository.softRemove(Group);

      return this.response({ data: deletedGroup });
    } catch (Exception) {
      if (Exception instanceof HttpErrors) {
        return this.error(Exception);
      } else {
        throw Exception;
      }
    }
  }

  @OpenAPI({
    description: "Update access group by Id",
    summary: "Update access group by Id",
  })
  @ResponseSchema(...Schema.NotPermissions)
  @ResponseSchema(...Schema.NotFound)
  @ResponseSchema(...Schema.NotAuth)
  @ResponseSchema(...Schema.NotValid)
  @ResponseSchema(Entities.AccessGroup)
  @HttpCode(201)
  @Auth()
  @Patch("/:group_id")
  public async updateGroup(
    @Body() dto: Dto.GroupUpdate,
    @Param("group_id") group_id: number,
    @CurrentUser() user: IUser
  ): Promise<Response> {
    try {
      const isAccess = AccessManager.checkPermission(user, AccessList.USER_CREATE);

      if (!isAccess) {
        throw new HttpErrors(ErrorsCodeEnum.USER_NOT_HAVE_PERMISSIONS);
      }

      const group = await this.groupRepository.findOne(group_id);

      if (!group) {
        throw new HttpErrors(ErrorsCodeEnum.ENTITY_NOT_FOUND);
      }

      if (dto.name) {
        const repeatedGroup = await this.groupRepository.findRepeated(group.id, dto.name);

        if (repeatedGroup) {
          throw new HttpErrors(ErrorsCodeEnum.VALIDATION_ERROR);
        }

        group.name = dto.name;

        await group.save();
      }

      const accesses = await this.accessRepository.findAccesses(dto.access_ids);

      await this.groupService.createRelation(accesses, group);

      const foundGroup = await this.groupRepository.findOne(group_id);

      return this.response({ data: foundGroup });
    } catch (Exception) {
      if (Exception instanceof HttpErrors) {
        return this.error(Exception);
      } else {
        throw Exception;
      }
    }
  }
}
