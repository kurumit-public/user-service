import {
  JsonController,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Ctx,
  HttpCode,
  CurrentUser,
  QueryParams,
} from "routing-controllers";
import { OpenAPI, ResponseSchema } from "routing-controllers-openapi";
import { Service } from "typedi";
import { InjectRepository } from "typeorm-typedi-extensions";
import { Context } from "koa";

import * as Mappers from "@mappers";
import * as Services from "@services";
import * as Repositories from "@repositories";
import * as Entities from "@entities";
import Controller from "@controllers/Controller";
import {
  ErrorsCodeEnum,
  HttpErrors,
  Auth,
  AccessList,
  AccessManager,
  filterUserUpdateDto,
} from "@helpers";
import { Schema } from "@schemas";

import { IUser, Response } from "@types";
import * as Dto from "@dto";
import * as Queries from "@queries";

@JsonController("/users")
@Service()
export default class UserController extends Controller {
  constructor(
    @InjectRepository() private userRepository: Repositories.User,
    private creatorService: Services.Creator,
    private userService: Services.User,
    private geoIpService: Services.GeoIp
  ) {
    super();
  }

  @OpenAPI({
    description: "Create new User",
    summary: "Create new User",
  })
  @ResponseSchema(...Schema.NotValid)
  @ResponseSchema(...Schema.NotFound)
  @ResponseSchema(...Schema.NotPermissions)
  @ResponseSchema(...Schema.NotAuth)
  @ResponseSchema(Entities.User)
  @HttpCode(201)
  @Auth()
  @Post()
  public async createUser(
    @Body() dto: Dto.User,
    @Ctx() ctx: Context,
    @CurrentUser() user: IUser
  ): Promise<Response> {
    const isAccess = AccessManager.checkPermission(user, AccessList.USER_CREATE);

    if (!isAccess) {
      throw new HttpErrors(ErrorsCodeEnum.USER_NOT_HAVE_PERMISSIONS);
    }

    const projectId = 1; // заменить когда будет project service

    try {
      const oldUser = await this.userRepository.findByEmail(dto.email);

      if (oldUser) {
        throw new HttpErrors(ErrorsCodeEnum.VALIDATION_ERROR);
      }

      const creator = await this.creatorService.getCreator(user.context.user.id);

      const newUser = await this.userService.createAndSaveUser(dto, projectId, creator.id);

      await this.geoIpService.saveGeoData(ctx, newUser.id);

      const foundUser = await this.userRepository.findOne(newUser.id);

      if (!foundUser) {
        throw new HttpErrors(ErrorsCodeEnum.ENTITY_NOT_FOUND);
      }

      const filteredUser = Mappers.User(foundUser);

      return this.response({ data: filteredUser });
    } catch (Exception) {
      if (Exception instanceof HttpErrors) {
        return this.error(Exception);
      } else {
        throw Exception;
      }
    }
  }

  @OpenAPI({
    description: "Get array of all users",
    summary: "Get array of all users",
  })
  @ResponseSchema(...Schema.NotAuth)
  @ResponseSchema(Entities.User, { isArray: true })
  @Auth()
  @Get()
  public async getAllUsers(
    @QueryParams({ validate: { whitelist: true } }) query: Queries.GetAllUsers,
    @CurrentUser() user: IUser
  ): Promise<Response> {
    try {
      const isAccess = AccessManager.checkPermission(user, AccessList.USER_WATCH);

      if (!isAccess) {
        throw new HttpErrors(ErrorsCodeEnum.USER_NOT_HAVE_PERMISSIONS);
      }

      const users = await this.userRepository.findAllUsersByQueryParams(query);

      const filteredUsers = Mappers.Users(users);

      return this.response({ data: filteredUsers });
    } catch (Exception) {
      if (Exception instanceof HttpErrors) {
        return this.error(Exception);
      } else {
        throw Exception;
      }
    }
  }

  @OpenAPI({
    description: "Get user by Id",
    summary: "Get user by Id",
  })
  @ResponseSchema(...Schema.NotAuth)
  @ResponseSchema(...Schema.NotFound)
  @ResponseSchema(Entities.User)
  @Auth()
  @Get("/:user_id")
  public async getUserById(
    @Param("user_id") user_id: number,
    @CurrentUser() user: IUser
  ): Promise<Response> {
    try {
      const isAccess = AccessManager.checkPermission(user, AccessList.USER_WATCH);
      const isCurrentUser = AccessManager.identifyCurrentUser(user, user_id);

      if (!isAccess && !isCurrentUser) {
        throw new HttpErrors(ErrorsCodeEnum.USER_NOT_HAVE_PERMISSIONS);
      }

      const foundUser = await this.userRepository.findOne(user_id);

      if (!foundUser) {
        throw new HttpErrors(ErrorsCodeEnum.ENTITY_NOT_FOUND);
      }

      const filteredUser = Mappers.User(foundUser);

      return this.response({ data: filteredUser });
    } catch (Exception) {
      if (Exception instanceof HttpErrors) {
        return this.error(Exception);
      } else {
        throw Exception;
      }
    }
  }

  @OpenAPI({
    description: "Delete user by Id",
    summary: "Delete user by Id",
  })
  @ResponseSchema(...Schema.NotAuth)
  @ResponseSchema(...Schema.NotFound)
  @ResponseSchema(...Schema.NotPermissions)
  @ResponseSchema(Entities.User)
  @Auth()
  @Delete("/:user_id")
  public async deleteUser(
    @Param("user_id") user_id: number,
    @CurrentUser() user: IUser
  ): Promise<Response> {
    try {
      const foundUser = await this.userRepository.findOne(user_id);

      if (!foundUser) {
        throw new HttpErrors(ErrorsCodeEnum.ENTITY_NOT_FOUND);
      }

      const isCurrentUser = AccessManager.identifyCurrentUser(user, user_id);
      const isAccess = AccessManager.checkPermission(user, AccessList.USER_DELETE);

      if (!isCurrentUser && !isAccess) {
        throw new HttpErrors(ErrorsCodeEnum.USER_NOT_HAVE_PERMISSIONS);
      }

      const deletedUser = await this.userRepository.softRemove(foundUser);

      if (!deletedUser) {
        throw new HttpErrors(ErrorsCodeEnum.ENTITY_NOT_FOUND);
      }

      const filteredUser = Mappers.User(deletedUser);

      return this.response({ data: filteredUser });
    } catch (Exception) {
      if (Exception instanceof HttpErrors) {
        return this.error(Exception);
      } else {
        throw Exception;
      }
    }
  }

  @OpenAPI({
    description: "Update user by Id",
    summary: "Update user by Id",
  })
  @ResponseSchema(...Schema.NotPermissions)
  @ResponseSchema(...Schema.NotFound)
  @ResponseSchema(...Schema.NotAuth)
  @ResponseSchema(...Schema.NotValid)
  @ResponseSchema(Entities.User)
  @HttpCode(201)
  @Auth()
  @Patch("/:user_id")
  public async updateUser(
    @Body() dto: Dto.UserUpdate,
    @Param("user_id") user_id: number,
    @CurrentUser() user: IUser
  ): Promise<Response> {
    const projectId = 1; // Заменить когда будет project service

    try {
      const foundUser = await this.userRepository.findOne(user_id);

      if (!foundUser) {
        throw new HttpErrors(ErrorsCodeEnum.ENTITY_NOT_FOUND);
      }

      const isCurrentUser = AccessManager.identifyCurrentUser(user, user_id);
      const isAccess = AccessManager.checkPermission(user, AccessList.USER_UPDATE);

      if (!isCurrentUser && !isAccess) {
        throw new HttpErrors(ErrorsCodeEnum.USER_NOT_HAVE_PERMISSIONS);
      }

      const filteredDto = filterUserUpdateDto(dto, isAccess);

      const updatedUser = await this.userService.updateUser(foundUser, filteredDto, projectId);

      if (!updatedUser) {
        throw new HttpErrors(ErrorsCodeEnum.ENTITY_NOT_FOUND);
      }

      const filteredUser = Mappers.User(updatedUser);

      return this.response({ data: filteredUser });
    } catch (Exception) {
      if (Exception instanceof HttpErrors) {
        return this.error(Exception);
      } else {
        throw Exception;
      }
    }
  }
}
