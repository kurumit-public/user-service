import { ErrorHandler } from "@helpers";
import { IErrorInfo, Response } from "@types";

export default abstract class Controller {
  protected response({ data, paginator }: Response): Response {
    return {
      status: "success",
      data: data === undefined ? undefined : JSON.parse(JSON.stringify(data)),
      paginator,
      timestamp: new Date().getTime(),
    };
  }

  protected error(errorInfo: IErrorInfo, data?: unknown): Response {
    throw new ErrorHandler(errorInfo, data);
  }
}
