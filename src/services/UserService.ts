import { Service } from "typedi";
import { InjectRepository } from "typeorm-typedi-extensions";
import { Repository } from "typeorm";

import * as Repositories from "@repositories";
import { Phone, User, NetworkingProfile, UserToAccess, Profile, NetworkingToTags } from "@entities";
import { Encryptor, ImageWorker } from "@helpers";

import * as Dto from "@dto";
import { IPicture } from "@types";

@Service()
export default class UserService {
  constructor(
    @InjectRepository() private userRepository: Repositories.User,
    @InjectRepository() protected tagRepository: Repositories.Tag,
    @InjectRepository(Phone) private phoneRepository: Repository<Phone>,
    @InjectRepository(NetworkingProfile)
    private networkingRepository: Repository<NetworkingProfile>,
    @InjectRepository(UserToAccess) private userToAccessRepository: Repository<UserToAccess>,
    @InjectRepository(Profile) private profileRepository: Repository<Profile>,
    @InjectRepository() private pictureRepository: Repositories.Picture
  ) {}

  private createNetworking(networkingTags: number[], projectId?: number): NetworkingProfile[] {
    const networkingProfiles: NetworkingProfile[] = [];

    if (!projectId) return networkingProfiles;

    const Networking = this.createNetworkingWithTags(networkingTags, projectId);
    networkingProfiles.push(Networking);

    return networkingProfiles;
  }

  private createNetworkingWithTags(
    networkingTags: number[],
    project_id: number
  ): NetworkingProfile {
    const networking = this.networkingRepository.create({ project_id, enabled: true, tags: [] });

    for (const tag of networkingTags) {
      const newTag = this.tagRepository.create({ tag_id: tag });

      networking.tags.push(newTag);
    }

    return networking;
  }

  private createAccesses(accesses: number[], projectId?: number): UserToAccess[] {
    const newAccesses: UserToAccess[] = [];

    for (const access of accesses) {
      const newAccess = this.userToAccessRepository.create({ access_id: access });

      if (projectId) newAccess.project_id = projectId;

      newAccesses.push(newAccess);
    }
    return newAccesses;
  }

  private async updateNetworking(
    networkingProfile: NetworkingProfile,
    dto: Dto.NetworkingProfile
  ): Promise<void> {
    const { enabled, tags } = dto;

    if (enabled !== undefined) {
      await this.networkingRepository.update(networkingProfile.id, { enabled });
    }

    if (!tags) return;

    const oldTags: number[] = [];

    const tagsForRemove: NetworkingToTags[] = [];

    networkingProfile.tags.forEach((tag) => {
      if (tags.includes(tag.tag_id)) {
        oldTags.push(tag.tag_id);
      } else {
        tagsForRemove.push(tag);
      }
    });

    await this.tagRepository.softRemove(tagsForRemove);

    const newTags = tags.filter((item) => !oldTags.includes(item));

    for (const tag of newTags) {
      const newTag = this.tagRepository.create({
        tag_id: tag,
        networking_profile_id: networkingProfile.id,
      });

      await this.tagRepository.save(newTag);
    }
  }

  private async updateAccesses(user: User, dto: number[], projectId?: number): Promise<void> {
    const newAccesses: number[] = [];

    const oldAccesses: number[] = [];

    user.accesses.forEach((access) => {
      if (dto.includes(access.access_id)) {
        newAccesses.push(access.access_id);
      } else {
        oldAccesses.push(access.id);
      }
    });

    await this.userToAccessRepository.softDelete(oldAccesses);

    const newDto = dto.filter((item) => !newAccesses.includes(item));

    for (const access of newDto) {
      const newAccess = this.userToAccessRepository.create({ access_id: access, user_id: user.id });

      if (projectId) newAccess.project_id = projectId;

      await this.userToAccessRepository.save(newAccess);
    }
  }

  private async updateProfile({ profile }: User, dto: Dto.ProfileUpdate): Promise<void> {
    const { picture, ...partialDto } = dto;

    await this.profileRepository.update(profile.id, partialDto);

    if (picture === undefined) return;

    if (picture === null) {
      await this.deletePicture(profile);
      return;
    }

    const pictures = await ImageWorker.savePictureOnServer(picture, profile.id);

    if (!profile.pictures?.length) {
      await this.createPicture(profile.id, pictures);
      return;
    }

    await this.updatePicture(profile, pictures);

    ImageWorker.deleteImage(profile.pictures);
  }

  private async createPicture(profile_id: number, pictures: IPicture[]) {
    for (const picture of pictures) {
      const newPicture = this.pictureRepository.create({
        ...picture,
        profile_id,
      });

      await this.pictureRepository.save(newPicture);
    }
  }

  private async updatePicture({ id, pictures }: Profile, newPictures: IPicture[]): Promise<void> {
    if (!pictures) return;

    const missedPictures = ImageWorker.findMissedPictures(pictures, newPictures);

    for (const picture of pictures) {
      const dto = newPictures.find((item) => item.resolution_width === picture.resolution_width);

      if (!dto) continue;

      await this.pictureRepository.update(picture.id, dto);
    }

    await this.createPicture(id, missedPictures);
  }

  private async deletePicture(profile: Profile): Promise<void> {
    if (!profile.pictures) return;

    for (const picture of profile.pictures) {
      await this.pictureRepository.softDelete(picture.id);
    }

    ImageWorker.deleteImage(profile.pictures);
  }

  private createProfile(profile?: Dto.Profile): Profile {
    if (!profile) return this.profileRepository.create();

    const { picture, ...profileDto } = profile;

    return this.profileRepository.create(profileDto);
  }

  public async findByEmailAndPass(email: string, password: string): Promise<User | undefined> {
    const hashPassword = Encryptor.createHash(password);

    return await this.userRepository.findByEmailAndPass(email, hashPassword);
  }

  public async createNetworkingProfile(
    user: User,
    projectId: number,
    newNetworkingTags: number[]
  ): Promise<void> {
    const lastNetworkingProfileId = user.networking_profiles.reduce((accum, current) =>
      accum.project_id > current.project_id ? accum : current
    ).id;

    const networkingTags = await this.tagRepository.findByProfileId(lastNetworkingProfileId);

    const tags = networkingTags.map((tag) => tag.tag_id);
    const newTags = tags.filter((tag) => newNetworkingTags.includes(tag));

    const dto: Dto.UserUpdate = {
      networking_profile: {
        tags: newTags,
      },
    };

    await this.updateUser(user, dto, projectId);
  }

  public async createAndSaveUser(
    { email, password, profile, phone, networking_tags, accesses }: Dto.User,
    projectId?: number,
    creatorId?: number
  ): Promise<User> {
    const user = this.userRepository.create();

    user.email = email;
    user.password = password;
    if (creatorId) user.creator_id = creatorId;
    if (phone) user.phone = this.phoneRepository.create({ ...phone, verified: false });
    if (accesses?.length && creatorId) user.accesses = this.createAccesses(accesses, projectId);

    if (networking_tags?.length) {
      user.networking_profiles = this.createNetworking(networking_tags, projectId);
    }

    user.profile = this.createProfile(profile);

    const newUser = await this.userRepository.save(user);

    if (profile?.picture) {
      const pictures = await ImageWorker.savePictureOnServer(profile.picture, newUser.profile.id);

      await this.createPicture(newUser.profile.id, pictures);
    }

    return newUser;
  }

  public async updateUser(
    user: User,
    dto: Dto.UserUpdate,
    projectId?: number
  ): Promise<User | undefined> {
    const { email, password, profile, phone, networking_profile, accesses } = dto;

    if (email) user.email = email;
    if (password) user.password = password;
    if (email || password) await user.save();
    if (profile) await this.updateProfile(user, profile);
    if (phone) await this.phoneRepository.update(user.phone.id, phone);
    if (accesses) await this.updateAccesses(user, accesses, projectId);

    if (projectId && networking_profile) {
      const networkingProfile = user.networking_profiles.find(
        (profile) => profile.project_id === projectId
      );

      if (!networkingProfile && networking_profile.tags) {
        const networking = this.createNetworkingWithTags(networking_profile.tags, projectId);

        networking.user_id = user.id;

        await networking.save();
      } else if (networkingProfile && networking_profile) {
        await this.updateNetworking(networkingProfile, networking_profile);
      }
    }

    return this.userRepository.findOne(user.id);
  }
}
