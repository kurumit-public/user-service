import { InjectRepository } from "typeorm-typedi-extensions";
import { Service } from "typedi";

import { Referral } from "@entities";
import { Repository } from "typeorm";

@Service()
export default class ReferralService {
  constructor(@InjectRepository(Referral) private referralRepository: Repository<Referral>) {}

  public async saveReferral(user_id: number, referral_id: number): Promise<void> {
    const newReferal = this.referralRepository.create({ user_id, referral_id });

    await newReferal.save();
  }
}
