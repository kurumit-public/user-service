import { Service } from "typedi";
import { InjectRepository } from "typeorm-typedi-extensions";
import { UpdateResult } from "typeorm";

import * as Repositories from "@repositories";
import { Project, User } from "@entities";

@Service()
export default class ProjectService {
  constructor(@InjectRepository() private projectRepository: Repositories.Project) {}

  public checkProject(user: User, projectId: number): boolean {
    return !!user.projects.find(
      (project) => project.project_id === projectId && project.user_id === user.id
    );
  }

  public async createRelation(
    user_id: number,
    project_id: number,
    confirmed?: boolean
  ): Promise<Project> {
    const relation = this.projectRepository.create({ project_id, user_id, confirmed });

    return await this.projectRepository.save(relation);
  }

  public async updateRelation(
    userId: number,
    projectId: number
  ): Promise<UpdateResult | undefined> {
    const relation = await this.projectRepository.findRelation(userId, projectId);

    if (!relation) return;

    return await this.projectRepository.update(relation.id, { confirmed: true });
  }
}
