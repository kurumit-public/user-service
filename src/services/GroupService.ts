import { Service } from "typedi";
import { InjectRepository } from "typeorm-typedi-extensions";

import * as Repositories from "@repositories";
import { Access, AccessGroup } from "@entities";

import * as Dto from "@dto";

@Service()
export default class GroupService {
  constructor(@InjectRepository() private groupRepository: Repositories.Group) {}

  public async createRelation(accesses: Access[], group: AccessGroup): Promise<void> {
    group.accesses.length = 0;

    for (const access of accesses) {
      group.accesses.push(access);
    }

    await this.groupRepository.save(group);
  }

  public async createGroup(dto: Dto.Group): Promise<AccessGroup> {
    const Group = this.groupRepository.create(dto);

    return await this.groupRepository.save(Group);
  }
}
