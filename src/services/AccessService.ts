import { Service } from "typedi";
import { InjectRepository } from "typeorm-typedi-extensions";

import * as Repositories from "@repositories";
import { Access, AccessGroup } from "@entities";

@Service()
export default class AccessService {
  constructor(@InjectRepository() private accessRepository: Repositories.Access) {}

  public async createRelation(accesses: Access[], group: AccessGroup): Promise<void> {
    for (const access of accesses) {
      access.groups.push(group);

      await this.accessRepository.save(access);
    }
  }
}
