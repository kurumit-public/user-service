import { Service } from "typedi";
import fs from "fs";
import jwt from "jsonwebtoken";

import { User } from "@entities";

@Service()
export default class CommonService {
  public signinJwt = ({ id, email, profile }: User, accesses: string[]): string => {
    const now = new Date();
    const privateKey = String(fs.readFileSync(".ssh/id_rsa"));

    const signedJwt = jwt.sign(
      {
        aud: "project",
        expiredAt: Math.round(new Date(now).setHours(new Date(now).getHours() + 24) / 1000),
        signedAt: Math.round(new Date(now).getTime() / 1000),
        context: {
          accesses,
          user: {
            id,
            email,
            firstname: profile.firstname,
            lastname: profile.lastname,
            pictures: profile.pictures,
          },
        },
      },
      privateKey
    );

    return signedJwt;
  };
}
