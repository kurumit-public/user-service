import { Service } from "typedi";
import { InjectRepository } from "typeorm-typedi-extensions";

import * as Repositories from "@repositories";
import { Creator } from "@entities";

@Service()
export default class CreatorService {
  constructor(@InjectRepository() private creatorRepository: Repositories.Creator) {}

  public async saveCreator(user_id: number): Promise<Creator> {
    const newCreator = this.creatorRepository.create({ user_id });

    return newCreator.save();
  }

  public async getCreator(userId: number): Promise<Creator> {
    let creator = await this.creatorRepository.findByUserId(userId);

    if (!creator) {
      creator = await this.saveCreator(userId);
    }

    return creator;
  }
}
