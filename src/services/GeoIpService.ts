import { Service } from "typedi";
import { InjectRepository } from "typeorm-typedi-extensions";
import { Repository } from "typeorm";
import geoip, { Lookup } from "geoip-lite";
import { Context } from "koa";

import { GeoIp } from "@entities";

@Service()
export default class GeoIpService {
  constructor(@InjectRepository(GeoIp) private geoIpRepository: Repository<GeoIp>) {}

  private async createGeoIp(geoIpData: Lookup, user_id: number): Promise<void> {
    const GeoIp = this.geoIpRepository.create({ ...geoIpData, user_id });

    await this.geoIpRepository.save(GeoIp);
  }

  public async saveGeoData(ctx: Context, user_id: number): Promise<void> {
    const geo_ip_data = geoip.lookup(ctx.request.ip);

    if (!geo_ip_data) {
      return;
    }

    await this.createGeoIp(geo_ip_data, user_id);
  }
}
