import { IsBoolean, IsOptional, IsPhoneNumber } from "class-validator";

export default class PhoneUpdateDto {
  @IsOptional()
  @IsPhoneNumber()
  readonly number!: string;

  @IsOptional()
  @IsBoolean()
  readonly verified!: boolean;
}
