import { IsInt, IsPositive, IsString } from "class-validator";

export default class GroupDto {
  @IsString()
  readonly name!: string;

  @IsPositive({ each: true })
  @IsInt({ each: true })
  readonly access_ids!: number[];
}
