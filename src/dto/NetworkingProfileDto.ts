import { IsBoolean, IsInt, IsOptional, IsPositive } from "class-validator";

export default class NetworkingProfileDto {
  @IsOptional()
  @IsBoolean()
  readonly enabled?: boolean;

  @IsOptional()
  @IsPositive({ each: true })
  @IsInt({ each: true })
  readonly tags?: number[];
}
