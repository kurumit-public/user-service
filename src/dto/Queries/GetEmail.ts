import { IsEmail } from "class-validator";

export default class GetEmail {
  @IsEmail()
  email!: string;
}
