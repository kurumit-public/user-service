import { IsIn, IsInt, IsOptional, IsPositive, IsString, MaxLength } from "class-validator";

const sortFields = ["id"];

export default class GetAllUsers {
  @IsOptional()
  @IsString()
  @MaxLength(128)
  fullname?: string;

  @IsOptional()
  @IsInt()
  @IsPositive()
  id?: number;

  @IsOptional()
  @IsString()
  @MaxLength(128)
  email?: string;

  @IsOptional()
  @IsString()
  @MaxLength(128)
  phone?: string;

  @IsOptional()
  @IsInt()
  @IsPositive()
  limit?: number;

  @IsOptional()
  @IsIn(sortFields)
  sort_field?: string;

  @IsOptional()
  @IsIn(["descend"])
  sort_type?: string;

  @IsOptional()
  @IsInt()
  @IsPositive()
  offset?: number;
}
