import { IsInt, IsOptional, IsPositive } from "class-validator";

export default class RegisterUser {
  @IsOptional()
  @IsInt()
  @IsPositive()
  inviter_id?: number;
}
