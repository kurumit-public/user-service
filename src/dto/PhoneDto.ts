import { IsPhoneNumber } from "class-validator";

export default class PhoneDto {
  @IsPhoneNumber()
  readonly number!: string;
}
