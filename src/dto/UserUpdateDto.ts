import { Type } from "class-transformer";
import {
  IsEmail,
  IsInt,
  IsOptional,
  IsPositive,
  IsString,
  MinLength,
  NotEquals,
  ValidateIf,
  ValidateNested,
} from "class-validator";

import { PhoneUpdate, ProfileUpdate, NetworkingProfile } from "@dto";

export default class UserUpdateDto {
  @ValidateIf((value) => value.email !== undefined)
  @NotEquals(null)
  @IsString()
  @IsEmail()
  readonly email?: string;

  @ValidateIf((value) => value.password !== undefined)
  @NotEquals(null)
  @IsString()
  @MinLength(8)
  readonly password?: string;

  @IsOptional()
  @ValidateNested()
  @Type(() => ProfileUpdate)
  readonly profile?: ProfileUpdate;

  @IsOptional()
  @ValidateNested()
  @Type(() => PhoneUpdate)
  readonly phone?: PhoneUpdate;

  @IsOptional()
  @ValidateNested()
  @Type(() => NetworkingProfile)
  readonly networking_profile?: NetworkingProfile;

  @IsOptional()
  @IsPositive({ each: true })
  @IsInt({ each: true })
  readonly accesses?: number[];
}
