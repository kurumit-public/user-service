import { IsBase64, IsString, MaxLength } from "class-validator";

import { getBase64Length, IsBase64Data } from "@helpers";

export default class PictureDto {
  @IsString()
  @MaxLength(128)
  @IsBase64Data()
  data!: string;

  @IsBase64()
  @MaxLength(getBase64Length(Number(process.env.MAX_IMAGE_SIZE) || 150))
  base64!: string;
}
