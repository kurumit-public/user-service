import { Type } from "class-transformer";
import { IsInt, IsOptional, IsPositive, IsString, Length, ValidateNested } from "class-validator";

import { Picture } from "./index";

export default class ProfileDto {
  @IsOptional()
  @IsString()
  @Length(2, 255)
  readonly firstname?: string;

  @IsOptional()
  @IsString()
  @Length(2, 255)
  readonly lastname?: string;

  @IsOptional()
  @IsString()
  @Length(2, 255)
  readonly patronymic?: string;

  @IsOptional()
  @IsPositive()
  @IsInt()
  readonly city_id?: number;

  @IsOptional()
  @IsPositive()
  @IsInt()
  readonly country_id?: number;

  @IsOptional()
  @IsPositive()
  @IsInt()
  readonly timezone_id?: number;

  @IsOptional()
  @ValidateNested()
  @Type(() => Picture)
  readonly picture?: Picture;

  @IsString()
  @Length(2, 2000)
  readonly description!: string;
}
