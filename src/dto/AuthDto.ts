import { IsEmail, IsString, MinLength } from "class-validator";

export default class AuthDto {
  @IsEmail()
  readonly email!: string;

  @IsString()
  @MinLength(8)
  readonly password!: string;
}
