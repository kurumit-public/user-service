import { IsInt, IsOptional, IsPositive, IsString } from "class-validator";

export default class GroupUpdateDto {
  @IsOptional()
  @IsString()
  readonly name!: string;

  @IsPositive({ each: true })
  @IsInt({ each: true })
  readonly access_ids!: number[];
}
