import { Type } from "class-transformer";
import {
  IsEmail,
  IsInt,
  IsOptional,
  IsPositive,
  IsString,
  MinLength,
  ValidateNested,
} from "class-validator";

import { Profile, Phone } from "@dto";

export default class UserDto {
  @IsString()
  @IsEmail()
  readonly email!: string;

  @IsString()
  @MinLength(8)
  readonly password!: string;

  @IsOptional()
  @ValidateNested()
  @Type(() => Profile)
  readonly profile?: Profile;

  @IsOptional()
  @ValidateNested()
  @Type(() => Phone)
  readonly phone?: Phone;

  @IsOptional()
  @IsPositive({ each: true })
  @IsInt({ each: true })
  readonly networking_tags?: number[];

  @IsOptional()
  @IsPositive({ each: true })
  @IsInt({ each: true })
  readonly accesses?: number[];
}
