import { EntityRepository, Repository } from "typeorm";

import { Project } from "@entities";
import { Service } from "typedi";

@Service()
@EntityRepository(Project)
export default class projectRepository extends Repository<Project> {
  public async findRelation(user_id: number, project_id: number): Promise<Project | undefined> {
    return await this.findOne({ where: { user_id, project_id } });
  }
}
