import { EntityRepository, Repository } from "typeorm";
import { Service } from "typedi";

import { Access } from "@entities";


@Service()
@EntityRepository(Access)
export default class AccessRepository extends Repository<Access> {
  public async findAccesses(ids: number[]): Promise<Access[]> {
    return await this.findByIds(ids, {
      relations: ["groups"],
    });
  }
}
