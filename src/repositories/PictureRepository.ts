import { EntityRepository, Repository } from "typeorm";
import { Service } from "typedi";

import { Picture } from "@entities";

@Service()
@EntityRepository(Picture)
export default class PictureRepository extends Repository<Picture> {
  
}
