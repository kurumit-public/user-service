import { EntityRepository, Repository } from "typeorm";
import { Service } from "typedi";

import { User } from "@entities";

import * as Queries from "@queries";

@Service()
@EntityRepository(User)
export default class UserRepository extends Repository<User> {
  public async findByEmail(email: string): Promise<User | undefined> {
    return await this.findOne({
      where: {
        email,
      },
      withDeleted: true,
    });
  }

  public async findByEmailAndPass(email: string, password: string): Promise<User | undefined> {
    return await this.findOne({
      where: {
        email,
        password,
      },
    });
  }

  public async findAllUsersByQueryParams({
    email,
    fullname,
    id,
    phone,
    limit,
    offset,
    sort_field = "id",
    sort_type,
  }: Queries.GetAllUsers): Promise<User[]> {
    const query = this.createQueryBuilder("user")
      .leftJoinAndSelect("user.profile", "profile")
      .leftJoinAndSelect("profile.pictures", "pictures")
      .leftJoinAndSelect("user.accesses", "accesses")
      .leftJoinAndSelect("accesses.access", "access")
      .leftJoinAndSelect("user.phone", "phone")
      .leftJoinAndSelect("user.creator", "creator")
      .leftJoinAndSelect("user.referrals", "referrals")
      .leftJoinAndSelect("user.networking_profiles", "networking_profiles")
      .leftJoinAndSelect("networking_profiles.tags", "tags")
      .leftJoinAndSelect("user.projects", "projects");

    if (fullname) {
      const userName = fullname.split(" ");

      const firstname = userName[0] !== "" ? userName[0] : null;
      const lastname = userName[1] !== "" ? userName[1] : null;

      query.where(
        `CONCAT(profile.firstname, ' ', profile.lastname) LIKE :firstname
         and CONCAT(profile.firstname, ' ', profile.lastname) LIKE :lastname`,
        {
          firstname: firstname ? `%${firstname}%` : null,
          lastname: lastname ? `%${lastname}%` : null,
        }
      );
    }
    if (id) {
      query.andWhere("user.id = :id", { id });
    }
    if (email) {
      query.andWhere("user.email = :email", { email });
    }
    if (phone) {
      query.andWhere("phone.number = :phone", { phone });
    }
    if (sort_field) {
      query.orderBy(`${[`user.${sort_field}`]}`, `${sort_type === "descend" ? "DESC" : "ASC"}`);
    }
    if (limit) {
      query.take(limit);
    }
    if (offset) {
      query.skip(offset);
    }

    return await query.getMany();
  }
}
