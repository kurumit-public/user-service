import { EntityRepository, Repository } from "typeorm";
import { Service } from "typedi";

import { NetworkingToTags } from "@entities";

@Service()
@EntityRepository(NetworkingToTags)
export default class NetworkingToTagsRepository extends Repository<NetworkingToTags> {
  public async findByProfileId(networking_profile_id: number): Promise<NetworkingToTags[]> {
    return await this.find({
      where: {
        networking_profile_id,
      },
    });
  }
}
