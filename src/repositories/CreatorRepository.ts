import { EntityRepository, Repository } from "typeorm";
import { Service } from "typedi";

import { Creator } from "@entities";


@Service()
@EntityRepository(Creator)
export default class CreatorRepository extends Repository<Creator> {
  public async findByUserId(user_id: number): Promise<Creator | undefined> {
    return await this.findOne({
      where: {
        user_id,
      },
    });
  }
}
