import { EntityRepository, Not, Repository } from "typeorm";
import { Service } from "typedi";

import { AccessGroup } from "@entities";

import * as Dto from "@dto";

@Service()
@EntityRepository(AccessGroup)
export default class GroupRepository extends Repository<AccessGroup> {
  public async findByName(dto: Dto.Group): Promise<AccessGroup | undefined> {
    return await this.findOne({
      where: {
        name: dto.name,
      },
    });
  }

  public async findRepeated(group_id: number, name: string): Promise<AccessGroup | undefined> {
    return await this.findOne({
      where: {
        name,
        id: Not(group_id),
      },
    });
  }
}
