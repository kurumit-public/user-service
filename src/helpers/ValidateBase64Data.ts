/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { registerDecorator } from "class-validator";

const IsBase64Data = () => {
  return function (object: any, propertyName: string): void {
    registerDecorator({
      name: "IsBase64Data",
      target: object.constructor,
      propertyName: propertyName,
      options: { message: "Incorrect base64 data format" },
      validator: {
        validate(value: string) {
        
          if (typeof value !== "string") return false;

          const imageException = value.split("/")[1];

          const allowedExceptions = ["png", "jpeg"];

          if (!allowedExceptions.includes(imageException)) {
            return false;
          }

          return true;
        },
      },
    });
  };
};

export default IsBase64Data;
