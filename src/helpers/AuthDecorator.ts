/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { getMetadataArgsStorage } from "routing-controllers";
import { getOpenAPIMetadata, OpenAPIParam, setOpenAPIMetadata } from "routing-controllers-openapi";

export function Auth(roleOrRoles?: string) {
  return function (target: any, propertyKey: string): void {
    getMetadataArgsStorage().responseHandlers.push({
      type: "authorized",
      target: propertyKey ? target.constructor : target,
      method: propertyKey,
      value: roleOrRoles,
    });

    const spec: OpenAPIParam = {
      security: [{ bearerAuth: [] }],
    };

    const currentMeta = getOpenAPIMetadata(target, propertyKey);
    setOpenAPIMetadata([spec, ...currentMeta], target, propertyKey);
  };
}
