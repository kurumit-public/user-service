import fs from "fs";
import Crypto from "crypto";

export default class Encryptor {
  private static generatePrivateKey(): string {
    return String(fs.readFileSync(".ssh/id_rsa"));
  }

  public static createHash(password: string): string {
    const privateKey = this.generatePrivateKey();
    return Crypto.createHmac("sha512", privateKey).update(password).digest("base64");
  }
}
