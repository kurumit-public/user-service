import crypto from "crypto";
import fs from "fs";
import sharp from "sharp";
import * as Sentry from "@sentry/node";

import { Picture } from "@entities";

import * as Dto from "@dto";
import { IPicture } from "@types";

export default class ImageWorker {
  static baseResolution = { width: 300, height: 300 };
  static pictureResolutions = [
    { width: 160, height: 160 },
    { width: 120, height: 120 },
    { width: 64, height: 64 },
    { width: 16, height: 16 },
  ];

  static getImgSize = (base64: string): number => {
    const y = base64.slice(-2) === "==" ? 2 : 1;

    const sizeInBytes = base64.length * (3 / 4) - y;

    return sizeInBytes / 1024;
  };

  static async savePictureOnServer(
    { data, base64 }: Dto.Picture,
    profile_id: number
  ): Promise<IPicture[]> {
    const now = new Date();

    const stringForHash = `${now.getTime()}`;

    const partialHash = crypto.createHash("md5").update(stringForHash).digest("hex");

    const extension = data.split("/")[1];

    const hash = `${profile_id}-${partialHash}`;

    const fileName = `${this.baseResolution.width}-${hash}.${extension}`;

    const size = this.getImgSize(base64);

    fs.writeFileSync(`uploads/${fileName}`, base64, { encoding: "base64" });

    const picture: IPicture = {
      hash,
      extension,
      size,
      resolution_width: this.baseResolution.width,
      resolution_height: this.baseResolution.height,
    };

    const pictures: IPicture[] = await this.resizeImg(
      fileName,
      hash,
      extension,
      this.pictureResolutions
    );

    pictures.push(picture);

    return pictures;
  }

  static async resizeImg(
    fileName: string,
    hash: string,
    extension: string,
    resolutions: { width: number; height: number }[]
  ): Promise<IPicture[]> {
    const pictures: IPicture[] = [];

    for (const resolution of resolutions) {
      const base64 = await sharp(`uploads/${fileName}`)
        .resize({ width: resolution.width, height: resolution.height })
        .toBuffer();

      const size = this.getImgSize(base64.toString("base64"));

      pictures.push({
        size,
        hash,
        extension,
        resolution_width: resolution.width,
        resolution_height: resolution.height,
      });

      fs.writeFileSync(`uploads/${resolution}-${hash}.${extension}`, base64, {
        encoding: "base64",
      });
    }

    return pictures;
  }

  static deleteImage(pictures: IPicture[]): void {
    for (const picture of pictures) {
      const { resolution_width, hash, extension } = picture;

      const fileName = `${resolution_width}-${hash}.${extension}`;

      fs.unlink(`uploads/${fileName}`, (Exception) => {
        if (Exception) {
          Sentry.captureException(Exception);
        }
      });
    }
  }

  static findMissedPictures(oldPictures: Picture[], newPictures: IPicture[]): IPicture[] {
    const oldResolutions = oldPictures.map((item) => item.resolution_width);

    return newPictures.filter((picture) => {
      return !oldResolutions.includes(picture.resolution_width);
    });
  }
}
