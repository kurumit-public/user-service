import { IErrorInfo } from "@types";

export class ErrorHandler extends Error {
  public statusCode: number;
  public data?: unknown;

  constructor({code, message}: IErrorInfo, data?: unknown) {
    super(message);
    this.statusCode = code;
    this.message = message;
    this.data = data;
  }
}
