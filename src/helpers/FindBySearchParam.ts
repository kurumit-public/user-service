import { FindOperator, Raw } from "typeorm";

import { ISearchParamArg } from "@types";

const createWhereFields = (
  // eslint-disable-next-line @typescript-eslint/ban-types
  searchParams: {},
  strictFields: string[]
): Record<string, string | number | FindOperator<unknown>> => {
  const params: Record<string, FindOperator<unknown>> = {};
  const strictParams: Record<string, string | number> = {};

  for (const key in searchParams) {
    if (Object.prototype.hasOwnProperty.call(searchParams, key)) {
      if (strictFields.includes(key)) {
        strictParams[key] = searchParams[key];
      } else {
        params[key] = Raw((alias) => `${alias} ILIKE '%${searchParams[key]}%'`);
      }
    }
  }

  let whereFields = {};

  if (Object.keys(params).length) {
    whereFields = { ...params };
  }

  if (Object.keys(strictParams).length) {
    whereFields = { ...whereFields, ...strictParams };
  }

  return whereFields;
};

const createOrder = (sort_field?: string, sort_type?: string) => {
  const order = {};

  const sortOrder = sort_type === "descend" ? "DESC" : "ASC";
  if (sort_field) {
    order[sort_field] = sortOrder;
  }

  return order;
};

const findBySearchParam = async <T>({
  repository,
  query,
  strictFields = [],
  queryConstructor,
}: ISearchParamArg<T>): Promise<T[]> => {
  try {
    const { limit, sort_type, sort_field, offset, ...params } = query;

    const order = createOrder(sort_field, sort_type);

    if (!queryConstructor) {
      const whereFields = createWhereFields(params, strictFields);

      return await repository.find({
        where: whereFields,
        take: limit,
        skip: offset,
        order,
      });
    }

    const { relations, whereFields } = queryConstructor(query);

    return await repository.find({
      relations,
      where: whereFields,
      take: limit,
      skip: offset,
      order,
    });
  } catch (Exception) {
    throw Exception;
  }
};

export default findBySearchParam;
