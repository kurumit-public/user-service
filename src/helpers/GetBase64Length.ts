// Accepts size in kilobytes
const getBase64Length = (size: number): number => {
  return Math.floor((size * 1024 * 4) / 3);
};

export default getBase64Length;