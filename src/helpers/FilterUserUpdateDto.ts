import * as Dto from "@dto";

const filterUserUpdateDto = (dto: Dto.UserUpdate, isAccess: boolean): Dto.UserUpdate => {
  if (isAccess) return dto;

  const { accesses, ...partDto } = dto;

  return partDto;
};

export default filterUserUpdateDto;
