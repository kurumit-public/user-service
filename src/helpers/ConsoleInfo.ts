import clc from "cli-color";
import packagejson from "@/package.json";

const info = [
  `[${packagejson.site}] ${packagejson.description}`,
  `Version: v${packagejson.version}`,
  `Author: ${packagejson.author}`,
  `Timezone - ${process.env.TZ}`,
];

const boxSize = Math.max(...info.map((str: string) => str.length));

const drawBorder = (): void => {
  console.info("#".repeat(boxSize + 5));
};

const drawDescription = (): void => {
  info.forEach((str: string) => {
    console.info("#", clc.blue(str), " ".repeat(boxSize - str.length), "#");
  });
};

export const drawInfo = (): void => {
  drawBorder();
  drawDescription();
  drawBorder();
};
