import { IUser } from "@types";
import { AccessList } from "@helpers";

export default class AccessManager {
  public static checkPermission(user: IUser, access: AccessList): boolean {
    return user.context.accesses.includes(access);
  }

  public static identifyCurrentUser(user: IUser, userId: number): boolean {
    return user.context.user.id === userId ? true : false;
  }
}
