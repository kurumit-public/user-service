export enum ErrorsCodeEnum {
  ENTITY_NOT_FOUND = "ENTITY_NOT_FOUND",
  USER_NOT_HAVE_PERMISSIONS = "USER_NOT_HAVE_PERMISSIONS",
  USER_NOT_AUTHORIZED = "USER_NOT_AUTHORIZED",
  VALIDATION_ERROR = "VALIDATION_ERROR",
}

export class HttpErrors {
  public declare code: number;
  public declare message: string;

  constructor(Code: ErrorsCodeEnum | string | number, message?: string) {
    if (typeof Code === "string") {
      this.errorGeneratorOfString(Code);
    }

    if (typeof Code === "number") {
      this.errorGeneratorOfNumber(Code, message);
    }
  }

  private errorGeneratorOfString(Code: ErrorsCodeEnum | string): void {
    switch (Code) {
      case ErrorsCodeEnum.ENTITY_NOT_FOUND:
        this.code = 404;
        this.message = Code;
        break;
      case ErrorsCodeEnum.USER_NOT_HAVE_PERMISSIONS:
        this.code = 403;
        this.message = Code;
        break;
      case ErrorsCodeEnum.USER_NOT_AUTHORIZED:
        this.code = 401;
        this.message = Code;
        break;
      case ErrorsCodeEnum.VALIDATION_ERROR:
        this.code = 422;
        this.message = Code;
        break;
      default:
        this.code = 500;
        this.message = Code;
    }
  }

  private errorGeneratorOfNumber(Code: number, message?: string): void {
    switch (Code) {
      case 404:
        this.code = 404;
        this.message = ErrorsCodeEnum.ENTITY_NOT_FOUND;
        break;
      case 403:
        this.code = 403;
        this.message = ErrorsCodeEnum.USER_NOT_HAVE_PERMISSIONS;
        break;
      case 401:
        this.code = 401;
        this.message = ErrorsCodeEnum.USER_NOT_AUTHORIZED;
        break;
      case 422:
        this.code = 422;
        this.message = ErrorsCodeEnum.VALIDATION_ERROR;
        break;
      default:
        this.code = 500;
        this.message = message || "Unexpected error";
    }
  }
}

