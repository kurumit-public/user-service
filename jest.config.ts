import type { Config } from "@jest/types";
import { pathsToModuleNameMapper } from "ts-jest";
import { compilerOptions } from "./tsconfig.json";

const config: Config.InitialOptions = {
  verbose: true,
  preset: "ts-jest",

  moduleNameMapper: pathsToModuleNameMapper(compilerOptions.paths, { prefix: "<rootDir>/" }),

  moduleFileExtensions: ["ts", "js"],
  testEnvironment: "node",

  testMatch: ["**/?(*.)+(spec|test).ts"],
};

export default config;
