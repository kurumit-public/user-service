import Crypto from "crypto";
import fs from "fs";
import clc from "cli-color";

const Keygen = async (): Promise<void> => {
  console.info(clc.blue("Keygen:"), clc.blue("Checking keys..."));
  try {
    if (fs.existsSync(".ssh/id_rsa")) {
      console.info(clc.blue("Keygen:"), clc.green("Keys found"));
    } else {
      console.error(clc.blue("Keygen:"), clc.red("Keys not exists"));
      console.info(clc.blue("Keygen:"), clc.blue("Generating keys..."));
      try {
        fs.mkdirSync(".ssh");
      } catch (Exception) {
        if (Exception) {
          if (Exception.code == "EEXIST") {
            console.info(clc.blue("Keygen:"), clc.green("'.ssh' directory exists"));
          } else {
            console.error(clc.blue("Keygen:"), clc.red("error"));
            console.error(Exception);
          }
        } else {
          console.info(clc.blue("Keygen:"), clc.green("'.ssh' directory created"));
        }
      }

      try {
        const { publicKey, privateKey } = Crypto.generateKeyPairSync("rsa", {
          modulusLength: 2048,
        });

        try {
          fs.writeFileSync(".ssh/id_rsa.pub", publicKey.export({ type: "pkcs1", format: "pem" }));
          console.info(clc.blue("Keygen:"), clc.green("Public key generated"));
        } catch (Exception) {
          console.error(clc.blue("Keygen:"), clc.red("Error while generating public key"));
          console.error(Exception);
        }

        try {
          fs.writeFileSync(".ssh/id_rsa", privateKey.export({ type: "pkcs1", format: "pem" }));
          console.info(clc.blue("Keygen:"), clc.green("Private key generated"));
        } catch (Exception) {
          console.error(clc.blue("Keygen:"), clc.red("Error while generating private key"));
          console.error(Exception);
        }

        await Keygen();
      } catch (Exception) {
        console.error(clc.blue("Keygen:"), clc.red("Error while generating keys"));
        console.error(Exception);
      }
    }
  } catch (Exception) {
    console.error(clc.blue("Keygen:"), clc.red("error"));
    console.error(Exception);
  }
};

export default Keygen;
