import Koa from "koa";
import bodyParser from "koa-bodyparser";
import cors from "@koa/cors";
import passport from "koa-passport";
import { useKoaServer, useContainer } from "routing-controllers";
import { Container } from "typedi";
import clc from "cli-color";
import * as Sentry from "@sentry/node";

import { getMetadataArgsStorage } from "routing-controllers";
import { routingControllersToSpec } from "routing-controllers-openapi";

import { koaSwagger } from "koa2-swagger-ui";
import { validationMetadatasToSchemas } from "class-validator-jsonschema";
import { defaultMetadataStorage } from "class-transformer/cjs/storage";

import Authorized from "@src/middlewares/Authorized";

export default class Webserver {
  private app: Koa;
  constructor() {
    this.app = new Koa();

    Sentry.init({
      dsn: process.env.SENTRY_DSN || "",
      tracesSampleRate: 1.0,
    });
    this.app.on("error", (err, ctx) => {
      Sentry.withScope((scope) => {
        scope.addEventProcessor((event) => {
          return Sentry.Handlers.parseRequest(event, ctx.request);
        });
        Sentry.captureException(err);
      });
    });

    this.app.use(passport.initialize());
    this.app.use(cors());
    this.app.use(bodyParser());

    useContainer(Container);

    useKoaServer(this.app, {
      routePrefix: "/api",
      controllers: [__dirname + "/../src/controllers/**/*.ts"],
      middlewares: [__dirname + "/../src/middlewares/common/**/*.ts"],

      authorizationChecker: Authorized.check,
      currentUserChecker: Authorized.user,
      defaultErrorHandler: false,
    });
  }

  public async run(port: number): Promise<void> {
    try {
      this.app.listen(port);

      console.info(clc.blue("Web server:"), clc.green("ok"), `${process.env.WEB_HOST}:${port}`);

      const storage = getMetadataArgsStorage();
      const schemas = validationMetadatasToSchemas({
        refPointerPrefix: "#/components/schemas/",
        classTransformerMetadataStorage: defaultMetadataStorage,
      });
      const spec = routingControllersToSpec(
        storage,
        {
          routePrefix: "/api",
          authorizationChecker: Authorized.check,
          currentUserChecker: Authorized.user,
        },
        {
          components: {
            schemas,
            securitySchemes: {
              bearerAuth: {
                type: "http",
                scheme: "bearer",
                bearerFormat: "JWT",
                name: "bearerAuth",
                in: "header",
              },
            },
          },
          info: { title: "User service", version: "0.0.1" },
        }
      );

      this.app.use(
        koaSwagger({
          oauthOptions: true,
          routePrefix: "/swagger",
          hideTopbar: true,
          title: "User | API",
          swaggerOptions: { spec },
        })
      );

      console.info(clc.blue("OpenAPI:"), clc.green("generated"));
    } catch (Exception) {
      Sentry.captureException(Exception);
      console.error(clc.blue("Web server:"), clc.red("error"), `${process.env.WEB_HOST}:${port}`);
      console.error(Exception);
    }
  }
}
