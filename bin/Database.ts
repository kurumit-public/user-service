import { Connection, createConnections, useContainer } from "typeorm";
import { Container } from "typeorm-typedi-extensions";
import clc from "cli-color";

export default class Database {
  private declare db: Connection[];

  public async createDb(): Promise<void> {
    try {
      useContainer(Container);

      this.db = await createConnections();

      console.info(
        clc.blue("Database:"),
        clc.green("ok"),
        `${process.env.DB_DATABASE}@${process.env.DB_HOST}:${process.env.DB_PORT}`
      );
    } catch (Exception) {
      console.error(
        clc.blue("Database:"),
        clc.red("error"),
        `${process.env.DB_DATABASE}@${process.env.DB_HOST}:${process.env.DB_PORT}`
      );
      console.error(Exception);
      process.exit();
    }
  }
}
