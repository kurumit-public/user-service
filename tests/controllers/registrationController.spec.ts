import { DeepPartial, Repository } from "typeorm";
import { createMockContext } from "@shopify/jest-koa-mocks";
import { Context } from "koa";

import {
  Profile,
  User,
  Phone,
  NetworkingProfile,
  NetworkingToTags,
  UserToAccess,
  Referral,
  GeoIp,
} from "@entities";
import {
  User as UserRepository,
  Project as ProjectRepository,
  Tag as TagRepository,
} from "@repositories";
import {
  User as UserService,
  Project as ProjectService,
  GeoIp as GeoIpService,
  Common as CommonService,
  Referral as ReferralService,
} from "@services";
import RegistrationController from "@controllers/api/RegistrationController";

import * as Dto from "@dto";

import {
  mockUserDto,
  mockRegistrationUser,
  mockReferral,
  mockQueryEmpty,
  mockQueryFilled,
} from "./mocks";

let ctx: Context;

let projectRepository: ProjectRepository;
let userRepository: UserRepository;
let phoneRepository: Repository<Phone>;
let tagRepository: TagRepository;
let networkingRepository: Repository<NetworkingProfile>;
let profileRepository: Repository<Profile>;
let userToAccessRepository: Repository<UserToAccess>;
let geoIpRepository: Repository<GeoIp>;
let referralRepository: Repository<Referral>;

let userService: UserService;
let projectService: ProjectService;
let geoIpService: GeoIpService;
let commonService: CommonService;
let referralService: ReferralService;

let registrationController: RegistrationController;

beforeAll(() => {
  ctx = createMockContext();

  projectRepository = new ProjectRepository();
  userRepository = new UserRepository();
  phoneRepository = new Repository<Phone>();
  profileRepository = new Repository<Profile>();
  tagRepository = new TagRepository();
  networkingRepository = new Repository<NetworkingProfile>();
  userToAccessRepository = new Repository<UserToAccess>();
  geoIpRepository = new Repository<GeoIp>();
  referralRepository = new Repository<Referral>();

  userService = new UserService(
    userRepository,
    tagRepository,
    phoneRepository,
    networkingRepository,
    userToAccessRepository,
    profileRepository
  );
  projectService = new ProjectService(projectRepository);
  geoIpService = new GeoIpService(geoIpRepository);
  commonService = new CommonService();
  referralService = new ReferralService(referralRepository);

  registrationController = new RegistrationController(
    userRepository,
    userService,
    projectService,
    geoIpService,
    commonService,
    referralService
  );
});

describe("Registration controller", () => {
  test("register user", async () => {
    const dbUser: DeepPartial<User> = {};

    let dbProfile: DeepPartial<Profile> = {};

    let dbPhone: DeepPartial<Phone> = {};

    const dbNetworking: DeepPartial<NetworkingProfile>[] = [];

    const dbTags: DeepPartial<NetworkingToTags>[] = [];

    const dbAccesses: DeepPartial<UserToAccess>[] = [];

    userRepository.findByEmail = jest
      .fn()
      .mockImplementationOnce((): DeepPartial<User> | undefined => undefined);

    userRepository.findOne = jest.fn().mockImplementation(
      (id: number): DeepPartial<User> | undefined =>
        // id === dbUser ? dbUser : undefined
        dbUser
    );

    userService.createAndSaveUser = jest
      .fn()
      .mockImplementationOnce((dto: Dto.User, projectId): DeepPartial<User> => {
        const { profile, phone, email, networking_tags } = dto;

        const { picture, ...profileDto } = profile as Dto.Profile;

        dbUser.id = 2;
        dbUser.email = email;

        dbProfile = { ...dbProfile, ...profileDto, picture: picture.base64, id: 2, user_id: 2 };

        dbPhone = { ...dbPhone, ...phone, id: 2, user_id: 2, verified: false };

        const Networking: DeepPartial<NetworkingProfile> = {};
        Networking.id = 2;
        Networking.user_id = 2;
        Networking.project_id = projectId;
        Networking.enabled = true;

        dbNetworking.push(Networking);

        if (networking_tags) {
          networking_tags.forEach((tag, idx: number) => {
            const networkingTag: DeepPartial<NetworkingToTags> = {};

            networkingTag.id = idx + 1;
            networkingTag.networking_profile_id = dbNetworking[0].id;
            networkingTag.tag_id = tag;

            dbTags.push(networkingTag);
          });
        }
        Networking.tags = dbTags;

        dbUser.profile = dbProfile;
        dbUser.phone = dbPhone;
        dbUser.networking_profiles = dbNetworking;
        dbUser.accesses = dbAccesses;

        return dbUser;
      });

    projectService.createRelation = jest.fn().mockImplementationOnce(() => undefined);

    referralService.saveReferral = jest.fn().mockImplementationOnce(() => undefined);

    geoIpService.saveGeoData = jest.fn().mockImplementationOnce((): void => undefined);

    const newUser = await registrationController.registerUser(mockUserDto, ctx, mockQueryEmpty);

    expect(newUser.data).toEqual(mockRegistrationUser);
  });

  test("register user with inviter", async () => {
    const dbUser: DeepPartial<User> = {};

    let dbProfile: DeepPartial<Profile> = {};

    let dbPhone: DeepPartial<Phone> = {};

    const dbNetworking: DeepPartial<NetworkingProfile>[] = [];

    const dbTags: DeepPartial<NetworkingToTags>[] = [];

    const dbAccesses: DeepPartial<UserToAccess>[] = [];

    const dbReferal: DeepPartial<Referral> = {};

    userRepository.findByEmail = jest
      .fn()
      .mockImplementationOnce((): DeepPartial<User> | undefined => undefined);

    userService.createAndSaveUser = jest
      .fn()
      .mockImplementationOnce((dto: Dto.User, hashPassword, projectId = 1): DeepPartial<User> => {
        const { profile, phone, email, networking_tags } = dto;

        const { picture, ...profileDto } = profile as Dto.Profile;

        dbUser.id = 2;
        dbUser.email = email;

        dbProfile = { ...dbProfile, ...profileDto, id: 2, user_id: 2 };

        dbPhone = { ...dbPhone, ...phone, id: 2, user_id: 2, verified: false };

        const Networking: DeepPartial<NetworkingProfile> = {};
        Networking.id = 2;
        Networking.user_id = 2;
        Networking.project_id = projectId;
        Networking.enabled = true;

        dbNetworking.push(Networking);

        if (networking_tags) {
          networking_tags.forEach((tag, idx: number) => {
            const networkingTag: DeepPartial<NetworkingToTags> = {};

            networkingTag.id = idx + 1;
            networkingTag.networking_profile_id = dbNetworking[0].id;
            networkingTag.tag_id = tag;

            dbTags.push(networkingTag);
          });
        }
        Networking.tags = dbTags;

        dbUser.profile = dbProfile;
        dbUser.phone = dbPhone;
        dbUser.networking_profiles = dbNetworking;
        dbUser.accesses = dbAccesses;

        return dbUser;
      });

    referralService.saveReferral = jest
      .fn()
      .mockImplementationOnce((inviterId: number, newUserId: number): void => {
        dbReferal.id = 1;
        dbReferal.user_id = inviterId;
        dbReferal.referral_id = newUserId;
      });

    geoIpService.saveGeoData = jest.fn().mockImplementationOnce((): void => undefined);

    userRepository.findOne = jest
      .fn()
      .mockImplementationOnce((): DeepPartial<User> | undefined => dbUser);

    const newUser = await registrationController.registerUser(mockUserDto, ctx, mockQueryFilled);

    expect(newUser.data).toEqual(mockRegistrationUser);
    expect(dbReferal).toEqual(mockReferral);
  });
});
