import { DeepPartial } from "typeorm";

import { Access, AccessGroup } from "@entities";
import { Group as GroupRepository, Access as AccessRepository } from "@repositories";
import { Access as AccessService, Group as GroupService } from "@services";
import AccessController from "@controllers/api/AccessGroupController";

import * as Dto from "@dto";

import {
  mockCurrentUser,
  mockAccessDto,
  mockAccess,
  mockUpdateAccessDto,
  mockUpdateAccess,
} from "./mocks";

let groupRepository: GroupRepository;
let accessRepository: AccessRepository;
let accessService: AccessService;
let groupService: GroupService;
let accessController: AccessController;

beforeAll(() => {
  groupRepository = new GroupRepository();
  accessRepository = new AccessRepository();
  accessService = new AccessService(accessRepository);
  groupService = new GroupService(groupRepository);
  accessController = new AccessController(
    groupRepository,
    accessRepository,
    accessService,
    groupService
  );
});

describe("AccessGroup controller", () => {
  let dbGroup: DeepPartial<AccessGroup> = {};

  const dbAccesses: DeepPartial<Access>[] = [];

  test("create new access group", async () => {
    groupRepository.findByName = jest
      .fn()
      .mockImplementationOnce((): AccessGroup | undefined => undefined);

    groupService.createGroup = jest
      .fn()
      .mockImplementationOnce((dto: Dto.Group): DeepPartial<AccessGroup> => {
        dbGroup = { ...dbGroup, id: 1, name: dto.name };

        return dbGroup;
      });

    accessRepository.findAccesses = jest.fn().mockImplementationOnce((): DeepPartial<Access>[] => {
      dbAccesses.push({ id: 1, name: "watch user", code: "USER_WATCH" });

      return dbAccesses;
    });

    accessService.createRelation = jest
      .fn()
      .mockImplementationOnce(
        (dbAccesses: DeepPartial<Access>[], dbGroup: DeepPartial<AccessGroup>): void => {
          dbGroup.accesses = dbAccesses;
        }
      );

    groupRepository.findOne = jest
      .fn()
      .mockImplementationOnce((id: number): DeepPartial<AccessGroup> | undefined =>
        dbGroup.id === id ? dbGroup : undefined
      );

    const foundGroup = await accessController.createGroup(mockAccessDto, mockCurrentUser);

    expect(foundGroup.data).toEqual(mockAccess);
  });
  test("update access group", async () => {
    groupRepository.findOne = jest
      .fn()
      .mockImplementation((id: number): DeepPartial<AccessGroup> | undefined =>
        dbGroup.id === id ? dbGroup : undefined
      );

    groupRepository.findRepeated = jest
      .fn()
      .mockImplementationOnce(
        (groupId: number, name: string): DeepPartial<AccessGroup> | undefined => {
          if (groupId !== dbGroup.id && name === dbGroup.name) {
            return dbGroup;
          } else {
            return undefined;
          }
        }
      );

    dbGroup.save = jest.fn().mockImplementationOnce(() => {
      dbGroup.name = mockUpdateAccessDto.name;
    });

    groupRepository.update = jest
      .fn()
      .mockImplementationOnce((groupId: number, dto: { name: string }): void => {
        if (dto.name && dbGroup.id === groupId) {
          dbGroup.name = dto.name;
        }
      });

    accessRepository.findAccesses = jest.fn().mockImplementationOnce(() => {
      dbAccesses.length = 0;

      dbAccesses.push(
        { id: 1, name: "watch user", code: "USER_WATCH" },
        { id: 2, name: "edit user", code: "USER_EDIT" }
      );
    });

    groupService.createRelation = jest.fn().mockImplementationOnce(() => {
      dbGroup.accesses = dbAccesses;
    });

    const foundGroup = await accessController.updateGroup(mockUpdateAccessDto, 1, mockCurrentUser);

    expect(foundGroup.data).toEqual(mockUpdateAccess);
  });
});
