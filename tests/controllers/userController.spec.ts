import { DeepPartial, Repository } from "typeorm";
import { createMockContext } from "@shopify/jest-koa-mocks";
import { Context } from "koa";

import {
  Profile,
  User,
  Phone,
  NetworkingProfile,
  NetworkingToTags,
  UserToAccess,
  Creator,
  GeoIp,
} from "@entities";
import {
  User as UserRepository,
  Creator as CreatorRepository,
  Tag as TagRepository,
} from "@repositories";
import { User as UserService, GeoIp as GeoIpService, Creator as CreatorService } from "@services";
import UserController from "@controllers/api/UserController";

import * as Dto from "@dto";

import { mockUserDto, mockUser, mockUpdateUserDto, mockUpdateUser, mockCurrentUser } from "./mocks";

let ctx: Context;

let userRepository: UserRepository;
let phoneRepository: Repository<Phone>;
let tagRepository: TagRepository;
let networkingRepository: Repository<NetworkingProfile>;
let profileRepository: Repository<Profile>;
let userToAccessRepository: Repository<UserToAccess>;
let geoIpRepository: Repository<GeoIp>;
let creatorRepository: CreatorRepository;

let userService: UserService;
let creatorService: CreatorService;
let geoIpService: GeoIpService;

let userController: UserController;

beforeAll(() => {
  ctx = createMockContext();

  userRepository = new UserRepository();
  phoneRepository = new Repository<Phone>();
  profileRepository = new Repository<Profile>();
  tagRepository = new TagRepository();
  networkingRepository = new Repository<NetworkingProfile>();
  userToAccessRepository = new Repository<UserToAccess>();
  geoIpRepository = new Repository<GeoIp>();

  creatorRepository = new CreatorRepository();

  userService = new UserService(
    userRepository,
    tagRepository,
    phoneRepository,
    networkingRepository,
    userToAccessRepository,
    profileRepository
  );
  creatorService = new CreatorService(creatorRepository);
  geoIpService = new GeoIpService(geoIpRepository);

  userController = new UserController(userRepository, creatorService, userService, geoIpService);
});

describe("User controller", () => {
  const dbUser: DeepPartial<User> = {};

  let dbProfile: DeepPartial<Profile> = {};

  let dbPhone: DeepPartial<Phone> = {};

  const dbNetworking: DeepPartial<NetworkingProfile>[] = [];

  const dbTags: DeepPartial<NetworkingToTags>[] = [];

  const dbAccesses: DeepPartial<UserToAccess>[] = [];

  const dbCreator: DeepPartial<Creator> = {};

  test("create user", async () => {
    userRepository.findByEmail = jest
      .fn()
      .mockImplementationOnce((email: string): DeepPartial<User> | undefined =>
        dbUser.email === email ? dbUser : undefined
      );

    creatorRepository.findByUserId = jest
      .fn()
      .mockImplementationOnce((id: number): DeepPartial<Creator> | undefined =>
        dbUser.id === id ? dbUser : undefined
      );

    creatorService.saveCreator = jest
      .fn()
      .mockImplementationOnce((creatorId: number): DeepPartial<Creator> => {
        dbCreator.id = 1;
        dbCreator.user_id = creatorId;

        return dbCreator;
      });

    userService.createAndSaveUser = jest
      .fn()
      .mockImplementationOnce((dto: Dto.User, projectId, creatorId): DeepPartial<User> => {
        const { profile, phone, email, networking_tags, accesses } = dto;

        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const { picture, ...profileDto } = profile as Dto.Profile;

        dbUser.id = 2;
        dbUser.email = email;
        dbUser.creator_id = creatorId;

        dbProfile = {
          ...dbProfile,
          ...profileDto,
          picture: picture.base64,
          id: dbUser.id,
          user_id: dbUser.id,
        };

        dbPhone = { ...dbPhone, ...phone, id: 2, user_id: dbUser.id, verified: false };

        const Networking: DeepPartial<NetworkingProfile> = {};
        Networking.id = 2;
        Networking.user_id = dbUser.id;
        Networking.project_id = projectId;
        Networking.enabled = true;

        dbNetworking.push(Networking);

        if (networking_tags) {
          networking_tags.forEach((tag, idx: number) => {
            const networkingTag: DeepPartial<NetworkingToTags> = {};

            networkingTag.id = idx + 1;
            networkingTag.networking_profile_id = Networking.id;
            networkingTag.tag_id = tag;

            dbTags.push(networkingTag);
          });
        }
        Networking.tags = dbTags;

        if (accesses) {
          accesses.forEach((access, idx: number) => {
            const newAccess: DeepPartial<UserToAccess> = {};

            newAccess.id = idx + 1;
            newAccess.user_id = dbUser.id;
            newAccess.project_id = projectId;
            newAccess.access_id = access;
            newAccess.access = {
              id: 1,
              name: "watch users",
              code: "USER_WATCH",
            };

            dbAccesses.push(newAccess);
          });
        }

        dbUser.profile = dbProfile;
        dbUser.phone = dbPhone;
        dbUser.networking_profiles = dbNetworking;

        dbUser.accesses = dbAccesses;

        return dbUser;
      });

    geoIpService.saveGeoData = jest.fn().mockImplementationOnce(() => undefined);

    userRepository.findOne = jest
      .fn()
      .mockImplementationOnce((id: number): DeepPartial<User> | undefined =>
        dbUser.id === id ? dbUser : undefined
      );

    const newUser = await userController.createUser(mockUserDto, ctx, mockCurrentUser);

    expect(newUser.data).toEqual(mockUser);
  });

  test("update user", async () => {
    userRepository.findOne = jest
      .fn()
      .mockImplementationOnce((): DeepPartial<User> | undefined => dbUser);

    userService.updateUser = jest
      .fn()
      .mockImplementationOnce((dbUser, mockUpdateUserDto: Dto.UserUpdate): DeepPartial<User> => {
        const { profile, phone, networking_profile } = mockUpdateUserDto;

        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const { picture, ...profileDto } = profile as Dto.ProfileUpdate;

        dbUser.email = mockUpdateUserDto.email ?? dbUser.email;

        if (profile) {
          dbProfile = { ...dbProfile, ...profileDto };
        }

        if (phone) {
          dbPhone = { ...dbPhone, ...phone };
        }

        if (networking_profile) {
          dbNetworking[0].enabled = networking_profile.enabled;

          dbTags.length = 0;

          networking_profile.tags?.forEach((tag: number, idx: number) => {
            const networkingTag: DeepPartial<NetworkingToTags> = {};

            networkingTag.id = idx + 1;
            networkingTag.networking_profile_id = dbNetworking[0].id;
            networkingTag.tag_id = tag;

            dbTags.push(networkingTag);
          });
        }

        dbUser.profile = dbProfile;
        dbUser.phone = dbPhone;

        return dbUser;
      });

    const updatedUser = await userController.updateUser(mockUpdateUserDto, 1, mockCurrentUser);

    expect(updatedUser.data).toEqual(mockUpdateUser);
  });
});
