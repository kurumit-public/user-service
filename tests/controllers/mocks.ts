import { DeepPartial } from "typeorm";

import { AccessGroup, Referral } from "@entities";
import { AccessList } from "@helpers";

import * as Dto from "@dto";
import * as Queries from "@queries";
import { IUser } from "@types";

export const mockCurrentUser: IUser = {
  aud: "",
  expiredAt: 1,
  signedAt: 1,
  context: {
    accesses: [
      AccessList.USER_CREATE,
      AccessList.USER_DELETE,
      AccessList.USER_UPDATE,
      AccessList.USER_WATCH,
    ],
    user: {
      id: 1,
      firstname: "",
      lastname: "",
      avatar: "",
      email: "",
    },
  },
};

export const mockPicture: Dto.Picture = {
  data: "image/png",
  base64: "adad/asdad/adadas/asdasdas/asdasd",
};

export const mockUser = {
  id: 2,
  email: "user@example.com",
  creator_id: 1,
  profile: {
    id: 2,
    user_id: 2,
    firstname: "Boba",
    lastname: "Biba",
    patronymic: "Bibovich",
    picture: mockPicture.base64,
    description: "Bibiboba",
    country_id: 1,
    city_id: 3,
    timezone_id: 2,
  },
  phone: {
    id: 2,
    user_id: 2,
    number: "5057",
    verified: false,
  },
  networking_profiles: [
    {
      id: 2,
      user_id: 2,
      project_id: 1,
      enabled: true,
      tags: [
        {
          id: 1,
          networking_profile_id: 2,
          tag_id: 1,
        },
        {
          id: 2,
          networking_profile_id: 2,
          tag_id: 2,
        },
        {
          id: 3,
          networking_profile_id: 2,
          tag_id: 5,
        },
      ],
    },
  ],
  accesses: ["USER_WATCH"],
};

export const mockUserDto: Dto.User = {
  email: "user@example.com",
  password: "stringst",
  profile: {
    firstname: "Boba",
    lastname: "Biba",
    patronymic: "Bibovich",
    city_id: 3,
    country_id: 1,
    timezone_id: 2,
    picture: mockPicture,
    description: "Bibiboba",
  },
  phone: {
    number: "5057",
  },
  networking_tags: [1, 2, 5],
  accesses: [1],
};

export const mockUpdateUserDto: Dto.UserUpdate = {
  email: "user5@example.com",
  password: "ststststst",
  profile: {
    firstname: "Boba",
    lastname: "Bibas",
    patronymic: "Bibovich",
    picture: mockPicture,
    description: "Bibiboba",
    country_id: 1,
    city_id: 7,
    timezone_id: 2,
  },
  phone: {
    number: "505744",
    verified: false,
  },
  networking_profile: {
    enabled: true,
    tags: [1, 2, 7],
  },
};

export const mockUpdateUser = {
  id: 2,
  email: "user5@example.com",
  creator_id: 1,
  profile: {
    id: 2,
    user_id: 2,
    firstname: "Boba",
    lastname: "Bibas",
    patronymic: "Bibovich",
    picture: mockPicture.base64,
    description: "Bibiboba",
    country_id: 1,
    city_id: 7,
    timezone_id: 2,
  },
  phone: {
    id: 2,
    user_id: 2,
    number: "505744",
    verified: false,
  },
  networking_profiles: [
    {
      id: 2,
      user_id: 2,
      project_id: 1,
      enabled: true,
      tags: [
        {
          id: 1,
          tag_id: 1,
          networking_profile_id: 2,
        },
        {
          id: 2,
          tag_id: 2,
          networking_profile_id: 2,
        },
        {
          id: 3,
          tag_id: 7,
          networking_profile_id: 2,
        },
      ],
    },
  ],
  accesses: ["USER_WATCH"],
};

export const mockAccessDto: Dto.Group = {
  name: "Watcher",
  access_ids: [1],
};

export const mockUpdateAccessDto: Dto.GroupUpdate = {
  access_ids: [1, 2],
  name: "Watcher and Editor",
};

export const mockAccess: DeepPartial<AccessGroup> = {
  id: 1,
  name: "Watcher",
  accesses: [
    {
      id: 1,
      name: "watch user",
      code: "USER_WATCH",
    },
  ],
};

export const mockUpdateAccess: DeepPartial<AccessGroup> = {
  id: 1,
  name: "Watcher and Editor",
  accesses: [
    {
      id: 1,
      name: "watch user",
      code: "USER_WATCH",
    },
    {
      id: 2,
      name: "edit user",
      code: "USER_EDIT",
    },
  ],
};

export const mockRegistrationUser = {
  id: 2,
  email: "user@example.com",
  profile: {
    id: 2,
    user_id: 2,
    firstname: "Boba",
    lastname: "Biba",
    patronymic: "Bibovich",
    picture: mockPicture.base64,
    description: "Bibiboba",
    country_id: 1,
    city_id: 3,
    timezone_id: 2,
  },
  phone: {
    id: 2,
    user_id: 2,
    number: "5057",
    verified: false,
  },
  networking_profiles: [
    {
      id: 2,
      user_id: 2,
      project_id: 1,
      enabled: true,
      tags: [
        {
          id: 1,
          networking_profile_id: 2,
          tag_id: 1,
        },
        {
          id: 2,
          networking_profile_id: 2,
          tag_id: 2,
        },
        {
          id: 3,
          networking_profile_id: 2,
          tag_id: 5,
        },
      ],
    },
  ],
  accesses: [],
};

export const mockReferral: DeepPartial<Referral> = {
  id: 1,
  user_id: 1,
  referral_id: 2,
};

export const mockQueryEmpty: Queries.RegisterUser = {
  inviter_id: undefined,
};

export const mockQueryFilled: Queries.RegisterUser = {
  inviter_id: 1,
};
