import { DeepPartial, MigrationInterface, QueryRunner } from "typeorm";

import Encryptor from "../../src/helpers/Encryptor";
import { User, UserToAccess, Profile } from "../../src/entities";
import { seedAccess } from "./1645095854581-seedAccess";

const accesses: number[] = seedAccess.map((access) => access.id);

const createAccesses = (accesses: number[]): UserToAccess[] => {
  const newAccesses: UserToAccess[] = [];

  for (const access of accesses) {
    const Access = new UserToAccess();

    Access.access_id = access;

    newAccesses.push(Access);
  }
  return newAccesses;
};

const createProfile = (): Profile => {
  return new Profile();
};

const seedUser: DeepPartial<User>[] = [
  {
    email: "admin@profi.travel",
    password: Encryptor.createHash("superpass"),
    accesses: createAccesses(accesses),
    profile: createProfile(),
  },
];

export class seedAdmin1646195545667 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.save(User, seedUser);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.delete(UserToAccess, accesses);

    await queryRunner.manager.delete(User, { email: "admin@profi.travel" });
  }
}
