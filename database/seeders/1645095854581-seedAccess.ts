import { MigrationInterface, QueryRunner } from "typeorm";

import { Access } from "../../src/entities";

export const seedAccess = [
  { id: 1, name: "watch user", code: "USER_WATCH" },
  { id: 2, name: "delete user", code: "USER_DELETE" },
  { id: 3, name: "update user", code: "USER_UPDATE" },
  { id: 4, name: "create user", code: "USER_CREATE" },
  { id: 5, name: "watch company", code: "COMPANY_WATCH" },
  { id: 6, name: "delete company", code: "COMPANY_DELETE" },
  { id: 7, name: "update company", code: "COMPANY_UPDATE" },
  { id: 8, name: "create company", code: "COMPANY_CREATE" },
  { id: 9, name: "watch stand", code: "STAND_WATCH" },
  { id: 10, name: "delete stand", code: "STAND_DELETE" },
  { id: 11, name: "update stand", code: "STAND_UPDATE" },
  { id: 12, name: "create stand", code: "STAND_CREATE" },
  { id: 13, name: "watch broadcast", code: "BROADCAST_WATCH" },
  { id: 14, name: "delete broadcast", code: "BROADCAST_DELETE" },
  { id: 15, name: "update broadcast", code: "BROADCAST_UPDATE" },
  { id: 16, name: "create broadcast", code: "BROADCAST_CREATE" },
  { id: 17, name: "chat admin", code: "CHAT_ADMIN" },
  { id: 18, name: "watch cutaway", code: "CUTAWAY_WATCH" },
  { id: 19, name: "delete cutaway", code: "CUTAWAY_DELETE" },
  { id: 20, name: "update cutaway", code: "CUTAWAY_UPDATE" },
  { id: 21, name: "create cutaway", code: "CUTAWAY_CREATE" },
  { id: 22, name: "watch project", code: "PROJECT_WATCH" },
  { id: 23, name: "delete project", code: "PROJECT_DELETE" },
  { id: 24, name: "update project", code: "PROJECT_UPDATE" },
  { id: 25, name: "create project", code: "PROJECT_CREATE" },
  { id: 26, name: "watch rating", code: "RATING_WATCH" },
  { id: 27, name: "delete rating", code: "RATING_DELETE" },
  { id: 28, name: "update rating", code: "RATING_UPDATE" },
  { id: 29, name: "create rating", code: "RATING_CREATE" },
];

export class seedGroup1645095854581 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.save(Access, seedAccess);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.delete(Access, seedAccess);
  }
}
