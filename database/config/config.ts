import { getMetadataArgsStorage } from "typeorm";
import * as Entities from "../../src/entities/index";

export default [
  {
    name: "default",
    type: process.env.DB_CONNECTION,
    host: process.env.DB_HOST,
    port: Number(process.env.DB_PORT),
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    synchronize: false,
    logging: false,
    entities: getMetadataArgsStorage().tables.map((table) => table.target),
    migrations: ["database/migrations/*.ts"],
    migrationsTableName: "typeormmeta",
    subscribers: ["database/subscribers/*.ts"],
    cli: {
      migrationsDir: "database/migrations",
    },
  },
  {
    name: "seed",
    type: process.env.DB_CONNECTION,
    host: process.env.DB_HOST,
    port: Number(process.env.DB_PORT),
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    synchronize: false,
    logging: false,
    entities: Object.values(Entities),
    migrations: ["database/seeders/*.ts"],
    migrationsTableName: "typeormseedmeta",
    cli: {
      migrationsDir: "database/seeders",
    },
  },
];
