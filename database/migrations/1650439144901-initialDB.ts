import {MigrationInterface, QueryRunner} from "typeorm";

export class initialDB1650439144901 implements MigrationInterface {
    name = 'initialDB1650439144901'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "users" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP, "email" character varying(256) NOT NULL, "password" character varying(256) NOT NULL, "creator_id" integer, CONSTRAINT "UQ_97672ac88f789774dd47f7c8be3" UNIQUE ("email"), CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "user_phones" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP, "user_id" integer NOT NULL, "phone" character varying(256) NOT NULL, "verified" boolean NOT NULL, CONSTRAINT "REL_96bd55026671b792bb3ce699ff" UNIQUE ("user_id"), CONSTRAINT "PK_975f5d595e466bdcbb7b0afc2b1" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "user_geo_ips" ("id" SERIAL NOT NULL, "user_id" integer NOT NULL, "country" character varying(256) NOT NULL, "timezone" character varying(256) NOT NULL, "city" character varying(256) NOT NULL, "region" character varying(256) NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_ff4cc160d9204cf987abf5069c3" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "user_profiles" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP, "user_id" integer NOT NULL, "firstname" character varying(256), "lastname" character varying(256), "patronymic" character varying(256), "avatar" character varying(256), "description" text, "country_id" integer, "city_id" integer, "timezone_id" integer, CONSTRAINT "REL_6ca9503d77ae39b4b5a6cc3ba8" UNIQUE ("user_id"), CONSTRAINT "PK_1ec6662219f4605723f1e41b6cb" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "networking_profiles" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP, "user_id" integer NOT NULL, "project_id" integer NOT NULL, "enabled" boolean NOT NULL, CONSTRAINT "PK_3aebeb30c484137ee1ba6034101" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_67379de65f0d527fb50f1f3440" ON "networking_profiles" ("project_id") `);
        await queryRunner.query(`CREATE TABLE "user_referals" ("id" SERIAL NOT NULL, "user_id" integer NOT NULL, "referal_id" integer NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_625594f019da01be2dd028d2c2b" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "user_creators" ("id" SERIAL NOT NULL, "user_id" integer NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "REL_f191bd528f2b21f531c077d6a1" UNIQUE ("user_id"), CONSTRAINT "PK_346ff7d0031cd86bb5c25060aa0" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "user_to_access" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP, "user_id" integer NOT NULL, "access_id" integer NOT NULL, "project_id" integer, CONSTRAINT "PK_96dc87cd6c21b5bcad7c89d8060" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_d2e83e24c181811fec46f80c7d" ON "user_to_access" ("project_id") `);
        await queryRunner.query(`CREATE TABLE "user_accesses" ("id" integer NOT NULL, "name" character varying(128) NOT NULL, "code" character varying(128) NOT NULL, CONSTRAINT "PK_e6e7cea5fa971af32caed86edec" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "access_groups" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP, "name" character varying(128) NOT NULL, CONSTRAINT "PK_ae49c2b86796456a21aaf57bbf2" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "networking_to_tags" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP, "tag_id" integer NOT NULL, "networking_profile_id" integer NOT NULL, CONSTRAINT "PK_62736b9e4191c9427dc9c42f615" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_a81d0051f4baa4f1cbef396353" ON "networking_to_tags" ("tag_id") `);
        await queryRunner.query(`CREATE TABLE "access_to_group" ("group_id" integer NOT NULL, "access_id" integer NOT NULL, CONSTRAINT "PK_800521a3fc9e3d5acf486a19e11" PRIMARY KEY ("group_id", "access_id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_55c84612970b882c5d4259b532" ON "access_to_group" ("group_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_458d617337a28a499385e36435" ON "access_to_group" ("access_id") `);
        await queryRunner.query(`ALTER TABLE "users" ADD CONSTRAINT "FK_eebaffddb3c6e049fa709e7de02" FOREIGN KEY ("creator_id") REFERENCES "user_creators"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user_phones" ADD CONSTRAINT "FK_96bd55026671b792bb3ce699ffd" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user_geo_ips" ADD CONSTRAINT "FK_54611d743d721da5364352da22f" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user_profiles" ADD CONSTRAINT "FK_6ca9503d77ae39b4b5a6cc3ba88" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "networking_profiles" ADD CONSTRAINT "FK_39d0b484426225562af26c1e3b2" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user_referals" ADD CONSTRAINT "FK_3fb689d66dc3f507d87ebd31af5" FOREIGN KEY ("referal_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user_referals" ADD CONSTRAINT "FK_5f51437a9c709954a8ca9ea3e68" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user_creators" ADD CONSTRAINT "FK_f191bd528f2b21f531c077d6a1c" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user_to_access" ADD CONSTRAINT "FK_9e36771be9cc6e670bdbd8e4ae4" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user_to_access" ADD CONSTRAINT "FK_86e4437f8ae63becdbb7dc6111c" FOREIGN KEY ("access_id") REFERENCES "user_accesses"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "networking_to_tags" ADD CONSTRAINT "FK_fe8f5a362f396fb68c8f7d00f07" FOREIGN KEY ("networking_profile_id") REFERENCES "networking_profiles"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "access_to_group" ADD CONSTRAINT "FK_55c84612970b882c5d4259b532a" FOREIGN KEY ("group_id") REFERENCES "access_groups"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "access_to_group" ADD CONSTRAINT "FK_458d617337a28a499385e364356" FOREIGN KEY ("access_id") REFERENCES "user_accesses"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "access_to_group" DROP CONSTRAINT "FK_458d617337a28a499385e364356"`);
        await queryRunner.query(`ALTER TABLE "access_to_group" DROP CONSTRAINT "FK_55c84612970b882c5d4259b532a"`);
        await queryRunner.query(`ALTER TABLE "networking_to_tags" DROP CONSTRAINT "FK_fe8f5a362f396fb68c8f7d00f07"`);
        await queryRunner.query(`ALTER TABLE "user_to_access" DROP CONSTRAINT "FK_86e4437f8ae63becdbb7dc6111c"`);
        await queryRunner.query(`ALTER TABLE "user_to_access" DROP CONSTRAINT "FK_9e36771be9cc6e670bdbd8e4ae4"`);
        await queryRunner.query(`ALTER TABLE "user_creators" DROP CONSTRAINT "FK_f191bd528f2b21f531c077d6a1c"`);
        await queryRunner.query(`ALTER TABLE "user_referals" DROP CONSTRAINT "FK_5f51437a9c709954a8ca9ea3e68"`);
        await queryRunner.query(`ALTER TABLE "user_referals" DROP CONSTRAINT "FK_3fb689d66dc3f507d87ebd31af5"`);
        await queryRunner.query(`ALTER TABLE "networking_profiles" DROP CONSTRAINT "FK_39d0b484426225562af26c1e3b2"`);
        await queryRunner.query(`ALTER TABLE "user_profiles" DROP CONSTRAINT "FK_6ca9503d77ae39b4b5a6cc3ba88"`);
        await queryRunner.query(`ALTER TABLE "user_geo_ips" DROP CONSTRAINT "FK_54611d743d721da5364352da22f"`);
        await queryRunner.query(`ALTER TABLE "user_phones" DROP CONSTRAINT "FK_96bd55026671b792bb3ce699ffd"`);
        await queryRunner.query(`ALTER TABLE "users" DROP CONSTRAINT "FK_eebaffddb3c6e049fa709e7de02"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_458d617337a28a499385e36435"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_55c84612970b882c5d4259b532"`);
        await queryRunner.query(`DROP TABLE "access_to_group"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_a81d0051f4baa4f1cbef396353"`);
        await queryRunner.query(`DROP TABLE "networking_to_tags"`);
        await queryRunner.query(`DROP TABLE "access_groups"`);
        await queryRunner.query(`DROP TABLE "user_accesses"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_d2e83e24c181811fec46f80c7d"`);
        await queryRunner.query(`DROP TABLE "user_to_access"`);
        await queryRunner.query(`DROP TABLE "user_creators"`);
        await queryRunner.query(`DROP TABLE "user_referals"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_67379de65f0d527fb50f1f3440"`);
        await queryRunner.query(`DROP TABLE "networking_profiles"`);
        await queryRunner.query(`DROP TABLE "user_profiles"`);
        await queryRunner.query(`DROP TABLE "user_geo_ips"`);
        await queryRunner.query(`DROP TABLE "user_phones"`);
        await queryRunner.query(`DROP TABLE "users"`);
    }

}
