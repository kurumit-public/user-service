import { MigrationInterface, QueryRunner } from "typeorm";

export class changePhoneFields1652330874969 implements MigrationInterface {
  name = "changePhoneFields1652330874969";

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "user_phones" RENAME COLUMN "phone" TO "number"`);
    await queryRunner.query(`ALTER TABLE "user_profiles" RENAME COLUMN "avatar" TO "picture"`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "user_phones" RENAME COLUMN "number" TO "phone"`);
    await queryRunner.query(`ALTER TABLE "user_profiles" RENAME COLUMN "picture" TO "avatar"`);
  }
}
