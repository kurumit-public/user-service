import {MigrationInterface, QueryRunner} from "typeorm";

export class addPictureTable1652440735372 implements MigrationInterface {
    name = 'addPictureTable1652440735372'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_referrals" DROP CONSTRAINT "FK_5f51437a9c709954a8ca9ea3e68"`);
        await queryRunner.query(`ALTER TABLE "user_referrals" DROP CONSTRAINT "FK_3fb689d66dc3f507d87ebd31af5"`);
        await queryRunner.query(`CREATE TABLE "profile_pictures" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP, "profile_id" integer NOT NULL, "hash" character varying NOT NULL, "extension" character varying NOT NULL, "size" numeric NOT NULL, "resolution_width" integer NOT NULL, "resolution_height" integer NOT NULL, CONSTRAINT "PK_55851331ec0d252521dd1f7cde2" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "user_profiles" DROP COLUMN "picture"`);
        await queryRunner.query(`CREATE SEQUENCE IF NOT EXISTS "user_referrals_id_seq" OWNED BY "user_referrals"."id"`);
        await queryRunner.query(`ALTER TABLE "user_referrals" ALTER COLUMN "id" SET DEFAULT nextval('"user_referrals_id_seq"')`);
        await queryRunner.query(`ALTER TABLE "user_referrals" ADD CONSTRAINT "FK_347ca8d65f4df0633d293854e35" FOREIGN KEY ("referral_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user_referrals" ADD CONSTRAINT "FK_4d434f568da1079713c316154aa" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "profile_pictures" ADD CONSTRAINT "FK_9afa65125560fc69b8e66a830d8" FOREIGN KEY ("profile_id") REFERENCES "user_profiles"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "profile_pictures" DROP CONSTRAINT "FK_9afa65125560fc69b8e66a830d8"`);
        await queryRunner.query(`ALTER TABLE "user_referrals" DROP CONSTRAINT "FK_4d434f568da1079713c316154aa"`);
        await queryRunner.query(`ALTER TABLE "user_referrals" DROP CONSTRAINT "FK_347ca8d65f4df0633d293854e35"`);
        await queryRunner.query(`ALTER TABLE "user_referrals" ALTER COLUMN "id" DROP DEFAULT`);
        await queryRunner.query(`DROP SEQUENCE "user_referrals_id_seq"`);
        await queryRunner.query(`ALTER TABLE "user_profiles" ADD "picture" character varying(256)`);
        await queryRunner.query(`DROP TABLE "profile_pictures"`);
        await queryRunner.query(`ALTER TABLE "user_referrals" ADD CONSTRAINT "FK_3fb689d66dc3f507d87ebd31af5" FOREIGN KEY ("referral_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user_referrals" ADD CONSTRAINT "FK_5f51437a9c709954a8ca9ea3e68" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
