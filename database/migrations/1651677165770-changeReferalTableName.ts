import { MigrationInterface, QueryRunner } from "typeorm";

export class changeReferalTableName1651677165770 implements MigrationInterface {
  name = "changeReferalTableName1651677165770";

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "user_referals" RENAME TO "user_referrals"`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "user_referrals" RENAME TO "user_referals"`);
  }
}
