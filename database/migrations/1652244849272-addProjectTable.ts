import { MigrationInterface, QueryRunner } from "typeorm";

export class addProjectTable1652244849272 implements MigrationInterface {
  name = "addProjectTable1652244849272";

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "user_referrals" RENAME COLUMN "referal_id" TO "referral_id"`
    );
    await queryRunner.query(
      `CREATE TABLE "user_projects" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP, "user_id" integer NOT NULL, "project_id" integer NOT NULL, "confirmed" boolean NOT NULL DEFAULT false, CONSTRAINT "PK_3d2a17149b660c4f9183241b911" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_4c6aaf014ba0d66a74bb552272" ON "user_projects" ("project_id") `
    );
    await queryRunner.query(
      `ALTER TABLE "user_projects" ADD CONSTRAINT "FK_86ef6061f6f13aa9252b12cbe87" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `CREATE SEQUENCE IF NOT EXISTS "user_referrals_id_seq" OWNED BY "user_referrals"."id"`
    );
    await queryRunner.query(`DROP SEQUENCE "user_referals_id_seq" CASCADE`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "user_referrals" RENAME COLUMN "referral_id" TO "referal_id"`
    );
    await queryRunner.query(`DROP INDEX "public"."IDX_4c6aaf014ba0d66a74bb552272"`);
    await queryRunner.query(`DROP TABLE "user_projects"`);
    await queryRunner.query(`DROP SEQUENCE "user_referrals_id_seq"`);
    await queryRunner.query(
      `CREATE SEQUENCE IF NOT EXISTS "user_referals_id_seq" OWNED BY "user_referals"."id"`
    );
  }
}
