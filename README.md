# User service

### Перед началом работы с контейнером выполнить:

```bash
echo 'export DOCKER_USER="$(id -u):$(id -g)"' >> ~/.bash_profile
source ~/.bash_profile
```

### Перед запуском приложения нужно выполнить:

```bash
cp .env.example .env
```
## Запустить приложение

```bash
docker-compose up
```

## Завершить работу приложения

```bash
docker-compose down
```
