process.env.TZ = "GMT";

import "reflect-metadata";
import Dotenv from "dotenv";
Dotenv.config();
import { drawInfo } from "@helpers";

drawInfo();

console.info();

import Database from "@bin/Database";
import Webserver from "@bin/Webserver";
import KeysGenerator from "@bin/KeysGenarator";

// Start service
(async () => {
  await KeysGenerator();
  await new Database().createDb();
  await new Webserver().run(Number(process.env.PORT || 8003));
  console.info();
})();
